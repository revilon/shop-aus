<?php

declare(strict_types=1);

namespace App\Form\Type\Admin;

use App\Entity\Taxonomy\Taxon;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;
use Sylius\Bundle\ResourceBundle\Doctrine\ORM\EntityRepository;
use Sylius\Component\Resource\Repository\RepositoryInterface;
use Symfony\Bridge\Doctrine\Form\DataTransformer\CollectionToArrayTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\Options;
use Symfony\Component\OptionsResolver\OptionsResolver;

final class TaxonChoiceType extends AbstractType
{
    /**
     * @var RepositoryInterface
     */
    private $taxonRepository;

    /**
     * @param EntityRepository $taxonRepository
     */
    public function __construct(EntityRepository $taxonRepository)
    {
        $this->taxonRepository = $taxonRepository;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        if ($options['multiple']) {
            $builder->addModelTransformer(new CollectionToArrayTransformer());
        }
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $choiceList = $this->taxonRepository->matching(
            new Criteria(Criteria::expr()->neq('code', Taxon::ROOT_TAXON_CODE))
        );

        $resolver->setDefaults([
            'choices' => static function (Options $options) use ($choiceList): Collection {
                return $choiceList;
            },
            'choice_value' => 'code',
            'choice_label' => 'name',
            'choice_translation_domain' => false,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getParent(): string
    {
        return ChoiceType::class;
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix(): string
    {
        return 'sylius_taxon_choice';
    }
}
