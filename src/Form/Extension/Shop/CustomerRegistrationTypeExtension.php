<?php

declare(strict_types=1);

namespace App\Form\Extension\Shop;

use Sylius\Bundle\CoreBundle\Form\Type\Customer\CustomerRegistrationType;
use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\IsTrue;

final class CustomerRegistrationTypeExtension extends AbstractTypeExtension
{
    /**
     * {@inheritDoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->add('privacy_policy_terms_of_use', CheckboxType::class, [
            'required' => true,
            'mapped' => false,
            'constraints' => [
                new IsTrue(['groups' => ['sylius']]),
            ],
        ]);
    }

    /**
     * {@inheritDoc}
     */
    public static function getExtendedTypes(): iterable
    {
        return [CustomerRegistrationType::class];
    }
}
