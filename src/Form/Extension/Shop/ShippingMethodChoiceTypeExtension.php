<?php

declare(strict_types=1);

namespace App\Form\Extension\Shop;

use App\Entity\Shipping\ShippingMethod;
use Sylius\Bundle\ShippingBundle\Form\Type\ShippingMethodChoiceType;
use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\ChoiceList\View\ChoiceView;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;

class ShippingMethodChoiceTypeExtension extends AbstractTypeExtension
{
    /**
     * {@inheritDoc}
     */
    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        /** @var ChoiceView $choice */
        foreach ($view->vars['choices'] as $key => $choice) {
            if ($choice->value === ShippingMethod::PICKUP_CODE) {
                continue;
            }

            if ($view->vars['shipping_costs'][$choice->value] <= 0) {
                $form->remove($key);
                unset($view->vars['choices'][$key]);
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    public static function getExtendedTypes(): iterable
    {
        return [ShippingMethodChoiceType::class];
    }
}
