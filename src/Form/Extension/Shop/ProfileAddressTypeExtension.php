<?php

declare(strict_types=1);

namespace App\Form\Extension\Shop;

use Sylius\Bundle\AddressingBundle\Form\Type\AddressType;
use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;

final class ProfileAddressTypeExtension extends AbstractTypeExtension
{
    /**
     * {@inheritDoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->remove('company');
        $builder->add('inlineAddress', TextType::class, [
            'required' => false,
            'mapped' => false,
            'label' => 'sylius.form.address.inline_address',
        ]);
    }

    /**
     * {@inheritDoc}
     */
    public function finishView(FormView $view, FormInterface $form, array $options): void
    {
        $nameField = $view->offsetGet('firstName');
        $surnameField = $view->offsetGet('lastName');
        $inlineAddressField = $view->offsetGet('inlineAddress');

        $nameField->vars['attr']['class'] = 'js-typeahead';
        $nameField->vars['row_attr']['class'] = 'typeahead__container';

        $surnameField->vars['attr']['class'] = 'js-typeahead';
        $surnameField->vars['row_attr']['class'] = 'typeahead__container';

        $inlineAddressField->vars['attr']['class'] = 'js-typeahead';
        $inlineAddressField->vars['row_attr']['class'] = 'typeahead__container';
    }

    /**
     * {@inheritDoc}
     */
    public static function getExtendedTypes(): iterable
    {
        return [AddressType::class];
    }
}
