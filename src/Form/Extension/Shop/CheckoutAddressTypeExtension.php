<?php

declare(strict_types=1);

namespace App\Form\Extension\Shop;

use Sylius\Bundle\CoreBundle\Form\Type\Checkout\AddressType;
use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\Validator\Constraints\IsTrue;

final class CheckoutAddressTypeExtension extends AbstractTypeExtension
{
    /**
     * {@inheritDoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->remove('differentBillingAddress');
        $builder->get('shippingAddress')->remove('company');
        $builder->get('shippingAddress')->add('inlineAddress', TextType::class, [
            'required' => false,
            'mapped' => false,
            'label' => 'sylius.form.address.inline_address',
        ]);
        $builder->get('billingAddress')->add('inlineAddress', TextType::class, [
            'required' => false,
            'mapped' => false,
            'label' => 'sylius.form.address.inline_address',
        ]);

        $builder->add('privacy_policy_terms_of_use', CheckboxType::class, [
            'required' => true,
            'mapped' => false,
            'constraints' => [
                new IsTrue(['groups' => ['sylius']]),
            ],
        ]);
    }

    /**
     * {@inheritDoc}
     */
    public function finishView(FormView $view, FormInterface $form, array $options): void
    {
        $nameField = $view->offsetGet('shippingAddress')->offsetGet('firstName');
        $surnameField = $view->offsetGet('shippingAddress')->offsetGet('lastName');
        $inlineAddressField = $view->offsetGet('shippingAddress')->offsetGet('inlineAddress');

        $nameField->vars['attr']['class'] = 'js-typeahead';
        $nameField->vars['row_attr']['class'] = 'typeahead__container';

        $surnameField->vars['attr']['class'] = 'js-typeahead';
        $surnameField->vars['row_attr']['class'] = 'typeahead__container';

        $inlineAddressField->vars['attr']['class'] = 'js-typeahead';
        $inlineAddressField->vars['row_attr']['class'] = 'typeahead__container';
    }

    /**
     * {@inheritDoc}
     */
    public static function getExtendedTypes(): iterable
    {
        return [AddressType::class];
    }
}
