<?php

declare(strict_types=1);

namespace App\Form\Extension\Admin;

use App\Entity\Taxonomy\Taxon;
use Sylius\Bundle\TaxonomyBundle\Form\Type\TaxonType;
use Sylius\Component\Taxonomy\Repository\TaxonRepositoryInterface;
use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

final class TaxonTypeExtension extends AbstractTypeExtension
{
    /**
     * @var TaxonRepositoryInterface
     */
    private $taxonRepository;

    /**
     * @param TaxonRepositoryInterface $taxonRepository
     */
    public function __construct(
        TaxonRepositoryInterface $taxonRepository
    ) {
        $this->taxonRepository = $taxonRepository;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->addEventListener(FormEvents::POST_SET_DATA, function (FormEvent $event): void {
            /** @var Taxon $taxon */
            $taxon = $event->getData();
            $form = $event->getForm();

            if ($taxon->getId()) {
                return;
            }

            $rootTaxon = $this->taxonRepository->findOneBy(['code' => Taxon::ROOT_TAXON_CODE]);

            $form->get('parent')->setData($rootTaxon);
        });
        $builder->addEventListener(FormEvents::PRE_SUBMIT, static function (FormEvent $event): void {
            $taxonData = $event->getData();

            foreach ($taxonData['translations'] as &$translationData) {
                $translationData['name'] = ucfirst($translationData['name']);
            }
            unset($translationData);

            $event->setData($taxonData);
        });
    }

    /**
     * {@inheritdoc}
     */
    public function getExtendedTypes(): iterable
    {
        return [TaxonType::class];
    }
}
