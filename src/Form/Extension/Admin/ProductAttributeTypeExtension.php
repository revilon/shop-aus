<?php

declare(strict_types=1);

namespace App\Form\Extension\Admin;

use App\Form\DataTransformer\TaxonsToTaxonAttributeListTransformer;
use App\Form\Type\Admin\TaxonChoiceType;
use Sylius\Bundle\ProductBundle\Form\Type\ProductAttributeType;
use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\FormBuilderInterface;

class ProductAttributeTypeExtension extends AbstractTypeExtension
{
    /**
     * @var TaxonsToTaxonAttributeListTransformer
     */
    private $taxonsToTaxonAttributeListTransformer;

    /**
     * @param TaxonsToTaxonAttributeListTransformer $taxonsToTaxonAttributeListTransformer
     */
    public function __construct(
        TaxonsToTaxonAttributeListTransformer $taxonsToTaxonAttributeListTransformer
    ) {
        $this->taxonsToTaxonAttributeListTransformer = $taxonsToTaxonAttributeListTransformer;
    }

    /**
     * {@inheritDoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->add('taxonList', TaxonChoiceType::class, [
            'label' => 'sylius.form.product_attribute.taxon',
            'multiple' => true,
            'expanded' => true,
        ]);

        $builder->addModelTransformer($this->taxonsToTaxonAttributeListTransformer);
    }

    /**
     * {@inheritdoc}
     */
    public function getExtendedTypes(): iterable
    {
        return [ProductAttributeType::class];
    }
}
