<?php

declare(strict_types=1);

namespace App\Form\Extension\Admin;

use App\Entity\Product\Product;
use Sylius\Bundle\ProductBundle\Form\Type\ProductVariantType;
use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Positive;

final class ProductVariantTypeExtension extends AbstractTypeExtension
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->remove('width');
        $builder->add('width', NumberType::class, [
            'required' => true,
            'label' => 'sylius.form.variant.width',
            'invalid_message' => 'sylius.product_variant.width.invalid',
            'constraints' => [
                new Positive(['groups' => ['sylius']]),
                new NotBlank(['groups' => ['sylius']]),
            ],
        ]);

        $builder->remove('height');
        $builder->add('height', NumberType::class, [
            'required' => true,
            'label' => 'sylius.form.variant.height',
            'invalid_message' => 'sylius.product_variant.height.invalid',
            'constraints' => [
                new Positive(['groups' => ['sylius']]),
                new NotBlank(['groups' => ['sylius']]),
            ],
        ]);

        $builder->remove('depth');
        $builder->add('depth', NumberType::class, [
            'required' => true,
            'label' => 'sylius.form.variant.depth',
            'invalid_message' => 'sylius.product_variant.depth.invalid',
            'constraints' => [
                new Positive(['groups' => ['sylius']]),
                new NotBlank(['groups' => ['sylius']]),
            ],
        ]);

        $builder->remove('weight');
        $builder->add('weight', NumberType::class, [
            'required' => true,
            'label' => 'sylius.form.variant.weight',
            'invalid_message' => 'sylius.product_variant.weight.invalid',
            'constraints' => [
                new Positive(['groups' => ['sylius']]),
                new NotBlank(['groups' => ['sylius']]),
            ],
        ]);

        $builder->remove('onHand');
        $builder->add('onHand', IntegerType::class, [
            'required' => true,
            'label' => 'sylius.form.variant.on_hand',
        ]);

        $builder->remove('tracked');
        $builder->add('tracked', CheckboxType::class, [
            'required' => true,
            'label' => 'sylius.form.variant.tracked',
        ]);

        $builder->addEventListener(FormEvents::POST_SET_DATA, static function (FormEvent $event): void {
            /** @var Product $product */
            $product = $event->getData();

            if ($product->getId()) {
                return;
            }

            $form = $event->getForm();
            $form->get('tracked')->setData(true);
            $form->get('onHand')->setData(1);
        });
    }

    /**
     * {@inheritdoc}
     */
    public function getExtendedTypes(): iterable
    {
        return [ProductVariantType::class];
    }
}
