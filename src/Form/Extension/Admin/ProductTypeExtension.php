<?php

declare(strict_types=1);

namespace App\Form\Extension\Admin;

use App\Component\SkuGenerator\SkuGenerator;
use App\Entity\Channel\Channel;
use App\Entity\Product\Product;
use App\Entity\Taxonomy\Taxon;
use Sylius\Bundle\ProductBundle\Form\Type\ProductType;
use Sylius\Component\Channel\Repository\ChannelRepositoryInterface;
use Sylius\Component\Core\Repository\ProductRepositoryInterface;
use Sylius\Component\Taxonomy\Repository\TaxonRepositoryInterface;
use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

final class ProductTypeExtension extends AbstractTypeExtension
{
    /**
     * @var ChannelRepositoryInterface
     */
    private $channelRepository;

    /**
     * @var TaxonRepositoryInterface
     */
    private $taxonRepository;

    /**
     * @var SkuGenerator
     */
    private $skuGenerator;

    /**
     * @var ProductRepositoryInterface
     */
    private $productRepository;

    /**
     * @param ChannelRepositoryInterface $channelRepository
     * @param TaxonRepositoryInterface $taxonRepository
     * @param SkuGenerator $skuGenerator
     * @param ProductRepositoryInterface $productRepository
     */
    public function __construct(
        ChannelRepositoryInterface $channelRepository,
        TaxonRepositoryInterface $taxonRepository,
        SkuGenerator $skuGenerator,
        ProductRepositoryInterface $productRepository
    ) {
        $this->channelRepository = $channelRepository;
        $this->taxonRepository = $taxonRepository;
        $this->skuGenerator = $skuGenerator;
        $this->productRepository = $productRepository;
    }

    /**
     * {@inheritDoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->addEventListener(FormEvents::POST_SET_DATA, function (FormEvent $event): void {
            /** @var Product $product */
            $product = $event->getData();

            if ($product->getId()) {
                return;
            }

            $form = $event->getForm();

            $form->get('channels')->setData($this->channelRepository->findBy([
                'code' => Channel::DEFAULT_CHANNEL_CODE
            ]));
            $form->get('mainTaxon')->setData($this->taxonRepository->findOneBy(['code' => Taxon::ROOT_TAXON_CODE]));

            $sku = $this->skuGenerator->generateSku($this->getExistSkuList());
            $form->get('code')->setData($sku);
        });
    }

    /**
     * {@inheritdoc}
     */
    public function getExtendedTypes(): iterable
    {
        return [ProductType::class];
    }

    /**
     * @return array
     */
    private function getExistSkuList(): array
    {
        $skuList = [];

        /** @var Product $product */
        foreach ($this->productRepository->findAll() as $product) {
            $sku = $product->getCode();
            $skuList[$sku] = $sku;
        }

        return array_values($skuList);
    }
}
