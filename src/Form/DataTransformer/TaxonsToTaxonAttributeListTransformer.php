<?php

declare(strict_types=1);

namespace App\Form\DataTransformer;

use App\Entity\Product\ProductAttribute;
use App\Entity\Taxonomy\Taxon;
use Doctrine\Common\Collections\ArrayCollection;
use Sylius\Component\Taxonomy\Repository\TaxonRepositoryInterface;
use Symfony\Component\Form\DataTransformerInterface;

class TaxonsToTaxonAttributeListTransformer implements DataTransformerInterface
{
    /**
     * @var TaxonRepositoryInterface
     */
    private $taxonRepository;

    /**
     * @param TaxonRepositoryInterface $taxonRepository
     */
    public function __construct(TaxonRepositoryInterface $taxonRepository)
    {
        $this->taxonRepository = $taxonRepository;
    }

    /**
     * {@inheritDoc}
     */
    public function transform($productAttribute)
    {
        return $productAttribute;
    }

    /**
     * @param ProductAttribute $attribute
     *
     * @return mixed
     */
    public function reverseTransform($attribute)
    {
        $actualTaxonList = $attribute->getTaxonList()->toArray();

        /** @var Taxon[] $taxonList */
        $taxonList = $this->taxonRepository->findAll();

        foreach ($taxonList as $taxon) {
            $taxon->removeAttribute($attribute);
        }

        /** @var Taxon $taxon */
        foreach ($actualTaxonList as $taxon) {
            $taxon->addAttribute($attribute);
        }
        $attribute->setTaxonList(new ArrayCollection($actualTaxonList));

        return $attribute;
    }
}
