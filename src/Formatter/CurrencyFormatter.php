<?php

declare(strict_types=1);

namespace App\Formatter;

class CurrencyFormatter
{
    /**
     * @param int $number
     *
     * @return string
     */
    public function formatRub(int $number): string
    {
        return number_format(
            $number / 100,
            2,
            ',',
            ' '
        ) . ' ₽';
    }
}
