<?php

declare(strict_types=1);

namespace App\UseCase\LatestProduct;

use App\Formatter\CurrencyFormatter;
use Doctrine\DBAL\DBALException;
use Liip\ImagineBundle\Imagine\Cache\CacheManager;
use Symfony\Component\Routing\RequestContext;

class LatestProductHandler
{
    /**
     * @var LatestProductManager
     */
    private $manager;

    /**
     * @var RequestContext
     */
    private $requestContext;

    /**
     * @var CacheManager
     */
    private $imageCacheManager;

    /**
     * @var CurrencyFormatter
     */
    private $currencyFormatter;

    /**
     * @param LatestProductManager $manager
     * @param RequestContext $requestContext
     * @param CacheManager $imageCacheManager
     * @param CurrencyFormatter $currencyFormatter
     */
    public function __construct(
        LatestProductManager $manager,
        RequestContext $requestContext,
        CacheManager $imageCacheManager,
        CurrencyFormatter $currencyFormatter
    ) {
        $this->manager = $manager;
        $this->requestContext = $requestContext;
        $this->imageCacheManager = $imageCacheManager;
        $this->currencyFormatter = $currencyFormatter;
    }

    /**
     * @param int $quantity
     *
     * @return array
     *
     * @throws DBALException
     */
    public function handle(int $quantity): array
    {
        $locale = $this->requestContext->getParameter('_locale');

        $latestProductList = $this->manager->getLatestProduct($quantity, $locale);

        $productIdList = array_column($latestProductList, 'id');
        $imagePathList = $this->manager->getImagePathList($productIdList);

        foreach ($latestProductList as &$latestProduct) {
            $productId = $latestProduct['id'];

            $price = $this->currencyFormatter->formatRub((int)$latestProduct['price']);
            $latestProduct['price'] = $price;

            if (isset($imagePathList[$productId])) {
                $imagePath = explode(',', $imagePathList[$productId])[0];
                $imagePath = parse_url($imagePath, PHP_URL_PATH);
                $imagePath = $this->imageCacheManager->getBrowserPath($imagePath, 'sylius_shop_product_thumbnail');
            } else {
                $imagePath = null;
            }
            $latestProduct['image_path'] = $imagePath;
        }

        return $latestProductList;
    }
}
