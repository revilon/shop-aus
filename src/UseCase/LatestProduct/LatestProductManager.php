<?php

declare(strict_types=1);

namespace App\UseCase\LatestProduct;

use App\Component\Manager\Executer\RowManager;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\DBALException;

class LatestProductManager
{
    /**
     * @var RowManager
     */
    private $manager;

    /**
     * @param RowManager $manager
     */
    public function __construct(
        RowManager $manager
    ) {
        $this->manager = $manager;
    }

    /**
     * @param int $quantity
     * @param string $locale
     *
     * @return array
     *
     * @throws DBALException
     */
    public function getLatestProduct(int $quantity, string $locale): array
    {
        $sql = <<<SQL
            select
                p.id,
                pt.slug,
                pt.locale,
                pt.name,
                cp.price
            from sylius_product p
            inner join sylius_product_translation pt on p.id = pt.translatable_id
            inner join sylius_product_variant pv on p.id = pv.product_id
            inner join sylius_channel_pricing cp on pv.id = cp.product_variant_id
            where 1
                and p.enabled = :is_enabled
                and pt.locale = :locale
            order by p.created_at desc
            limit %s
SQL;

        $sql = sprintf($sql, $quantity);

        $stmt = $this->manager->getConnection()->executeQuery($sql, [
            'is_enabled' => true,
            'locale' => $locale,
        ]);

        return $stmt->fetchAll();
    }

    /**
     * @param array $productIdList
     *
     * @return array
     *
     * @throws DBALException
     */
    public function getImagePathList(array $productIdList): array
    {
        $sql = <<<SQL
            select
                owner_id product_id,
                GROUP_CONCAT(path) image_path_list
            from sylius_product_image pi
            where owner_id in (:product_id_list)
            group by owner_id
SQL;

        $stmt = $this->manager->getConnection()->executeQuery(
            $sql,
            [
                'product_id_list' => $productIdList,
            ],
            [
                'product_id_list' => Connection::PARAM_STR_ARRAY,
            ]
        );

        return $this->manager->getResultPairList($stmt, 'product_id', 'image_path_list');
    }
}
