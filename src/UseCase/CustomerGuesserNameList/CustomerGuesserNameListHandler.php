<?php

declare(strict_types=1);

namespace App\UseCase\CustomerGuesserNameList;

use App\Component\Dadata\Exception\DadataException;
use App\Component\Dadata\Provider\DadataProvider;

class CustomerGuesserNameListHandler
{
    /**
     * @var CustomerGuesserNameListManager
     */
    private $manager;

    /**
     * @var DadataProvider
     */
    private $dadataProvider;

    /**
     * @param CustomerGuesserNameListManager $manager
     * @param DadataProvider $dadataProvider
     */
    public function __construct(CustomerGuesserNameListManager $manager, DadataProvider $dadataProvider)
    {
        $this->manager = $manager;
        $this->dadataProvider = $dadataProvider;
    }

    /**
     * @param string $searchText
     *
     * @return array
     *
     * @throws DadataException
     */
    public function handle(string $searchText): array
    {
        $customerGuesserNameList = [];

        $customerGuesserNameDtoList = $this->dadataProvider->getFullNameDtoList($searchText);

        foreach ($customerGuesserNameDtoList as $customerGuesserNameDto) {
            $customerGuesserNameList[] = $customerGuesserNameDto->getName();
        }

        return array_values(array_filter(array_unique($customerGuesserNameList)));
    }
}
