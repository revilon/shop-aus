<?php

declare(strict_types=1);

namespace App\UseCase\Refund;

use Payum\Core\Model\GatewayConfigInterface;
use Payum\Core\Payum;
use Payum\Core\Request\Refund;
use Sylius\Component\Core\Model\PaymentInterface;
use Sylius\Component\Core\Model\PaymentMethodInterface;

class RefundHandler
{
    /**
     * @var Payum
     */
    private $payum;

    /**
     * @param Payum $payum
     */
    public function __construct(
        Payum $payum
    ) {
        $this->payum = $payum;
    }

    public function handle(PaymentInterface $payment): void
    {
        /** @var PaymentMethodInterface $paymentMethod */
        $paymentMethod = $payment->getMethod();

        /** @var GatewayConfigInterface $gatewayConfig */
        $gatewayConfig = $paymentMethod->getGatewayConfig();

        $gatewayName = $gatewayConfig->getGatewayName();

        $this->payum->getGateway($gatewayName)->execute(new Refund($payment));
    }
}
