<?php

declare(strict_types=1);

namespace App\UseCase\TaxonList;

use App\Component\Manager\Executer\RowManager;
use Doctrine\DBAL\DBALException;

class TaxonListManager
{
    /**
     * @var RowManager
     */
    private $manager;

    /**
     * @param RowManager $manager
     */
    public function __construct(RowManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * @return array
     *
     * @throws DBALException
     */
    public function getTaxonList(): array
    {
        $sql = <<<SQL
            select
                t.id,
                t.parent_id,
                t.code,
                tt.name,
                tt.slug,
                tt.locale
            from sylius_taxon t
            inner join sylius_taxon_translation tt on t.id = tt.translatable_id
            order by t.position
SQL;

        $stmt = $this->manager->getConnection()->executeQuery($sql);

        return $this->manager->getResultList($stmt, 'id');
    }
}
