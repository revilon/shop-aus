<?php

declare(strict_types=1);

namespace App\UseCase\TaxonList;

use Doctrine\DBAL\DBALException;

class TaxonListHandler
{
    /**
     * @var TaxonListManager
     */
    private $manager;

    /**
     * @param TaxonListManager $manager
     */
    public function __construct(TaxonListManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * @return array
     *
     * @throws DBALException
     */
    public function handle(): array
    {
        $taxonList = $this->manager->getTaxonList();

        $taxonList = $this->createTree($taxonList);

        return $taxonList;
    }

    /**
     * @param $list
     * @param null $parentId
     *
     * @return array
     */
    public function createTree(&$list, $parentId = null): array
    {
        $tree = [];

        foreach ($list as $key => $eachNode) {
            if ($eachNode['parent_id'] === $parentId) {
                $eachNode['children'] = $this->createTree($list, $eachNode['id']);

                $tree[] = $eachNode;

                unset($list[$key]);
            }
        }

        return $tree;
    }
}
