<?php

declare(strict_types=1);

namespace App\UseCase\PaymentPendingProcessing;

use Exception;
use Payum\Core\Model\GatewayConfigInterface;
use Payum\Core\Payum;
use Psr\Log\LoggerInterface;
use Sylius\Bundle\PayumBundle\Request\GetStatus;
use Sylius\Component\Core\Model\PaymentMethodInterface;
use Sylius\Component\Core\Repository\PaymentRepositoryInterface;
use Sylius\Component\Payment\Model\PaymentInterface;

class PaymentPendingProcessingHandler
{
    /**
     * @var Payum
     */
    private $payum;

    /**
     * @var PaymentRepositoryInterface
     */
    private $paymentRepository;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @param Payum $payum
     * @param PaymentRepositoryInterface $paymentRepository
     * @param LoggerInterface $logger
     */
    public function __construct(
        Payum $payum,
        PaymentRepositoryInterface $paymentRepository,
        LoggerInterface $logger
    ) {
        $this->payum = $payum;
        $this->paymentRepository = $paymentRepository;
        $this->logger = $logger;
    }

    public function handle(): void
    {
        /** @var PaymentInterface[] $paymentList */
        $paymentList = $this->paymentRepository->findBy([
            'state' => PaymentInterface::STATE_PROCESSING,
        ]);

        foreach ($paymentList as $payment) {
            $paymentDetails = $payment->getDetails();

            $paymentId = $paymentDetails['payment_id'] ?? null;

            if (!$paymentId) {
                $this->logger->error("Payment '{$payment->getId()}' don't have payment_id");

                $payment->setState(PaymentInterface::STATE_FAILED);

                continue;
            }

            try {
                /** @var PaymentMethodInterface $paymentMethod */
                $paymentMethod = $payment->getMethod();

                /** @var GatewayConfigInterface $gatewayConfig */
                $gatewayConfig = $paymentMethod->getGatewayConfig();

                $gatewayName = $gatewayConfig->getGatewayName();

                $this->payum->getGateway($gatewayName)->execute(new GetStatus($payment));
            } catch (Exception $exception) {
                $this->logger->error("Payment '{$payment->getId()}' error: {$exception->getMessage()}");

                $payment->setState(PaymentInterface::STATE_FAILED);
            }

            $this->logger->debug("Payment '{$payment->getId()}' completed");
        }
    }
}
