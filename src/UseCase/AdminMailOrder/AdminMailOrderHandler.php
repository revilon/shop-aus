<?php

declare(strict_types=1);

namespace App\UseCase\AdminMailOrder;

use App\Entity\Channel\Channel;
use App\Enum\EmailVariantEnum;
use Sylius\Component\Channel\Repository\ChannelRepositoryInterface;
use Sylius\Component\Core\Model\OrderInterface;
use Sylius\Component\Mailer\Sender\SenderInterface;

class AdminMailOrderHandler
{
    /**
     * @var SenderInterface
     */
    private $sender;

    /**
     * @var ChannelRepositoryInterface
     */
    private $channelRepository;

    /**
     * @param SenderInterface $sender
     * @param ChannelRepositoryInterface $channelRepository
     */
    public function __construct(
        SenderInterface $sender,
        ChannelRepositoryInterface $channelRepository
    ) {
        $this->sender = $sender;
        $this->channelRepository = $channelRepository;
    }

    /**
     * @param OrderInterface $order
     */
    public function handle(OrderInterface $order): void
    {
        /** @var Channel $channel */
        $channel = $this->channelRepository->findOneByCode(Channel::DEFAULT_CHANNEL_CODE);

        $toEmail = $channel->getContactEmail();

        $this->sender->send(EmailVariantEnum::ADMIN_ORDER, [$toEmail], ['order' => $order]);
    }
}
