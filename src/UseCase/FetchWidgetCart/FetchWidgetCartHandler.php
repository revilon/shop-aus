<?php

declare(strict_types=1);

namespace App\UseCase\FetchWidgetCart;

use App\Entity\Channel\Channel;
use App\Formatter\CurrencyFormatter;
use Doctrine\DBAL\DBALException;
use RuntimeException;
use Sylius\Component\Core\Model\ShopUserInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpFoundation\Session\Storage\SessionStorageInterface;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Authorization\Voter\AuthenticatedVoter;

class FetchWidgetCartHandler
{
    /**
     * @var FetchWidgetCartManager
     */
    private $manager;

    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * @var AuthorizationCheckerInterface
     */
    private $authorizationChecker;

    /**
     * @var SessionStorageInterface
     */
    private $session;

    /**
     * @var CurrencyFormatter
     */
    private $currencyFormatter;

    /**
     * @var RequestContext
     */
    private $requestContext;

    /**
     * @param FetchWidgetCartManager $manager
     * @param TokenStorageInterface $tokenStorage
     * @param AuthorizationCheckerInterface $authorizationChecker
     * @param SessionInterface $session
     * @param CurrencyFormatter $currencyFormatter
     * @param RequestContext $requestContext
     */
    public function __construct(
        FetchWidgetCartManager $manager,
        TokenStorageInterface $tokenStorage,
        AuthorizationCheckerInterface $authorizationChecker,
        SessionInterface $session,
        CurrencyFormatter $currencyFormatter,
        RequestContext $requestContext
    ) {
        $this->manager = $manager;
        $this->tokenStorage = $tokenStorage;
        $this->authorizationChecker = $authorizationChecker;
        $this->session = $session;
        $this->currencyFormatter = $currencyFormatter;
        $this->requestContext = $requestContext;
    }

    /**
     * @param Request $request
     *
     * @return array
     *
     * @throws DBALException
     */
    public function handle(Request $request): array
    {
        $locale = $this->requestContext->getParameter('_locale');

        $channel = $this->getChannel($request);

        $userId = $this->getUserId();

        if ($userId) {
            $cart = $this->manager->getOrderByUserId($userId, (int)$channel['id']);
        }

        if (isset($cart)) {
            $orderItemList = $this->manager->getOrderItemList((int)$cart['id'], $locale);

            return $this->format($cart, $orderItemList);
        }

        $cardId = $this->session->get("_sylius.cart.$channel[code]");

        if ($cardId) {
            $cart = $this->manager->getOrderById($cardId, (int)$channel['id']);
        }

        if (isset($cart)) {
            $orderItemList = $this->manager->getOrderItemList((int)$cardId, $locale);

            return $this->format($cart, $orderItemList);
        }

        $this->session->remove("_sylius.cart.$channel[code]");

        return $this->format($cart ?? [], $orderItemList ?? []);
    }

    /**
     * @param Request $request
     *
     * @return array|null
     *
     * @throws DBALException
     */
    private function getChannel(Request $request): ?array
    {
        $channel = $this->manager->getChannelByCode(Channel::DEFAULT_CHANNEL_CODE);

        if (isset($channel)) {
            return $channel;
        }

        $requestChannelCode = $request->query->get('_channel_code') ?: $request->cookies->get('_channel_code');

        if ($requestChannelCode !== null) {
            $channel = $this->manager->getChannelByCode($requestChannelCode);

            if (isset($channel)) {
                return $channel;
            }
        }

        $channel = $this->manager->getChannelByHostname($request->getHost());

        if (isset($channel)) {
            return $channel;
        }

        $channel = $this->manager->getChannelFirst();

        if (isset($channel)) {
            return $channel;
        }

        throw new RuntimeException('Not found channel for fetch cart');
    }

    /**
     * @return int|null
     */
    private function getUserId(): ?int
    {
        $token = $this->tokenStorage->getToken();

        if (null === $token) {
            return null;
        }

        $user = $token->getUser();

        $isAuthenticatedRemembered = $this->authorizationChecker->isGranted(
            AuthenticatedVoter::IS_AUTHENTICATED_REMEMBERED
        );

        if ($user instanceof ShopUserInterface && $isAuthenticatedRemembered) {
            return $user->getId();
        }

        return null;
    }

    /**
     * @param array $cart
     * @param array $orderItemList
     *
     * @return array
     */
    private function format(array $cart, array $orderItemList): array
    {
        $itemsTotal = $cart['items_total'] ?? 0;

        $cart['items_total'] = $this->currencyFormatter->formatRub((int)$itemsTotal);

        foreach ($orderItemList as $key => $orderItem) {
            $unitPrice = $this->currencyFormatter->formatRub((int)$orderItem['unit_price']);

            $cart['item_list'][$key] = $orderItem;
            $cart['item_list'][$key]['unit_price'] = $unitPrice;
        }

        if (!isset($cart['item_list'])) {
            $cart['item_list'] = [];
        }

        return $cart;
    }
}
