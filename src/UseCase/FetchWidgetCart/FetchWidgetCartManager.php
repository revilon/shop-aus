<?php

declare(strict_types=1);

namespace App\UseCase\FetchWidgetCart;

use App\Component\Manager\Executer\RowManager;
use App\Entity\Order\Order;
use Doctrine\DBAL\DBALException;

class FetchWidgetCartManager
{
    /**
     * @var RowManager
     */
    private $manager;

    /**
     * @param RowManager $manager
     */
    public function __construct(RowManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * @param string $code
     *
     * @return array|null
     *
     * @throws DBALException
     */
    public function getChannelByCode(string $code): ?array
    {
        $sql = <<<SQL
            select
                c.id,
                c.code
            from sylius_channel c
            where 1
                and code = :code
SQL;

        $stmt = $this->manager->getConnection()->executeQuery($sql, [
            'code' => $code,
        ]);

        return $stmt->fetch() ?: null;
    }

    /**
     * @param string $hostname
     *
     * @return array|null
     *
     * @throws DBALException
     */
    public function getChannelByHostname(string $hostname): ?array
    {
        $sql = <<<SQL
            select
                c.id,
                c.code
            from sylius_channel c
            where 1
                and hostname = :hostname
            limit 1
SQL;

        $stmt = $this->manager->getConnection()->executeQuery($sql, [
            'hostname' => $hostname,
        ]);

        return $stmt->fetch() ?: null;
    }

    /**
     * @return array|null
     *
     * @throws DBALException
     */
    public function getChannelFirst(): ?array
    {
        $sql = <<<SQL
            select
                c.id,
                c.code
            from sylius_channel c
            order by created_at desc
            limit 1
SQL;

        $stmt = $this->manager->getConnection()->executeQuery($sql);

        return $stmt->fetch() ?: null;
    }

    /**
     * @param int $userId
     * @param int $channelId
     *
     * @return array|null
     *
     * @throws DBALException
     */
    public function getOrderByUserId(int $userId, int $channelId): ?array
    {
        $sql = <<<SQL
            select
               o.id,
               o.items_total
            from sylius_order o
            inner join sylius_customer c on o.customer_id = c.id
            inner join sylius_shop_user su on c.id = su.customer_id
            where 1
                and su.id = :user_id
                and o.id = :channel_id
                and o.state = :state_cart
            order by o.created_at desc
            limit 1
SQL;

        $stmt = $this->manager->getConnection()->executeQuery($sql, [
            'user_id' => $userId,
            'channel_id' => $channelId,
            'state_cart' => Order::STATE_CART,
        ]);

        return $stmt->fetch() ?: null;
    }

    /**
     * @param int $orderId
     * @param int $channelId
     *
     * @return array|null
     *
     * @throws DBALException
     */
    public function getOrderById(int $orderId, int $channelId): ?array
    {
        $sql = <<<SQL
            select
                o.id,
                o.items_total
            from sylius_order o
            where 1
                and o.id = :order_id
                and o.channel_id = :channel_id
                and o.state = :state_cart
            limit 1
SQL;

        $stmt = $this->manager->getConnection()->executeQuery($sql, [
            'order_id' => $orderId,
            'channel_id' => $channelId,
            'state_cart' => Order::STATE_CART,
        ]);

        return $stmt->fetch() ?: null;
    }

    /**
     * @param int $orderId
     * @param string $locale
     *
     * @return array
     *
     * @throws DBALException
     */
    public function getOrderItemList(int $orderId, string $locale): array
    {
        $sql = <<<SQL
            select
                oi.id,
                oi.quantity,
                oi.unit_price,
                pt.name
            from sylius_order_item oi
            inner join sylius_product_variant pv on oi.variant_id = pv.id
            inner join sylius_product p on pv.product_id = p.id
            inner join sylius_product_translation pt on p.id = pt.translatable_id
            where 1
                and pt.locale = :locale
                and oi.order_id = :order_id
SQL;

        $stmt = $this->manager->getConnection()->executeQuery($sql, [
            'order_id' => $orderId,
            'locale' => $locale,
        ]);

        return $stmt->fetchAll();
    }
}
