<?php

declare(strict_types=1);

namespace App\UseCase\CustomerGuesserAddressList;

use App\Component\Manager\Executer\RowManager;

class CustomerGuesserAddressListManager
{
    /**
     * @var RowManager
     */
    private $manager;

    /**
     * @param RowManager $manager
     */
    public function __construct(RowManager $manager)
    {
        $this->manager = $manager;
    }
}
