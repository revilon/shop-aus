<?php

declare(strict_types=1);

namespace App\UseCase\CustomerGuesserAddressList;

use App\Component\Dadata\Exception\DadataException;
use App\Component\Dadata\Provider\DadataProvider;

class CustomerGuesserAddressListHandler
{
    /**
     * @var CustomerGuesserAddressListManager
     */
    private $manager;

    /**
     * @var DadataProvider
     */
    private $dadataProvider;

    /**
     * @param CustomerGuesserAddressListManager $manager
     * @param DadataProvider $dadataProvider
     */
    public function __construct(CustomerGuesserAddressListManager $manager, DadataProvider $dadataProvider)
    {
        $this->manager = $manager;
        $this->dadataProvider = $dadataProvider;
    }

    /**
     * @param string $searchText
     *
     * @return array
     *
     * @throws DadataException
     */
    public function handle(string $searchText): array
    {
        $addressList = [];

        $addressDtoList = $this->dadataProvider->getAddressDtoList($searchText);

        foreach ($addressDtoList as $addressDto) {
            $addressList[] = $addressDto->getAddressInline();
        }

        return array_values(array_filter(array_unique($addressList)));
    }
}
