<?php

declare(strict_types=1);

namespace App\UseCase\CustomerGuesserSurnameList;

use App\Component\Dadata\Exception\DadataException;
use App\Component\Dadata\Provider\DadataProvider;

class CustomerGuesserSurnameListHandler
{
    /**
     * @var CustomerGuesserSurnameListManager
     */
    private $manager;

    /**
     * @var DadataProvider
     */
    private $dadataProvider;

    /**
     * @param CustomerGuesserSurnameListManager $manager
     * @param DadataProvider $dadataProvider
     */
    public function __construct(CustomerGuesserSurnameListManager $manager, DadataProvider $dadataProvider)
    {
        $this->manager = $manager;
        $this->dadataProvider = $dadataProvider;
    }

    /**
     * @param string $searchText
     *
     * @return array
     *
     * @throws DadataException
     */
    public function handle(string $searchText): array
    {
        $customerGuesserSurnameList = [];

        $customerGuesserSurnameDtoList = $this->dadataProvider->getFullNameDtoList($searchText);

        foreach ($customerGuesserSurnameDtoList as $customerGuesserSurnameDto) {
            $customerGuesserSurnameList[] = $customerGuesserSurnameDto->getSurname();
        }

        return array_values(array_unique($customerGuesserSurnameList));
    }
}
