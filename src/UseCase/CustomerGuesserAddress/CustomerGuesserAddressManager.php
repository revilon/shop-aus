<?php

declare(strict_types=1);

namespace App\UseCase\CustomerGuesserAddress;

use App\Component\Manager\Executer\RowManager;

class CustomerGuesserAddressManager
{
    /**
     * @var RowManager
     */
    private $manager;

    /**
     * @param RowManager $manager
     */
    public function __construct(RowManager $manager)
    {
        $this->manager = $manager;
    }
}
