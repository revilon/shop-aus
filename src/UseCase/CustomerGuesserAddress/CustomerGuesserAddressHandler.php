<?php

declare(strict_types=1);

namespace App\UseCase\CustomerGuesserAddress;

use App\Component\Dadata\Exception\DadataException;
use App\Component\Dadata\Provider\DadataProvider;
use RuntimeException;

class CustomerGuesserAddressHandler
{
    /**
     * @var CustomerGuesserAddressManager
     */
    private $manager;

    /**
     * @var DadataProvider
     */
    private $dadataProvider;

    /**
     * @param CustomerGuesserAddressManager $manager
     * @param DadataProvider $dadataProvider
     */
    public function __construct(CustomerGuesserAddressManager $manager, DadataProvider $dadataProvider)
    {
        $this->manager = $manager;
        $this->dadataProvider = $dadataProvider;
    }

    /**
     * @param string $searchText
     *
     * @return array
     *
     * @throws DadataException
     */
    public function handle(string $searchText): array
    {
        $addressDtoList = $this->dadataProvider->getAddressDtoList($searchText);

        if (!$addressDtoList) {
            throw new RuntimeException("Dadata provider can't find address for query '$searchText'");
        }

        $addressDto = $addressDtoList[0];

        $region[] = $addressDto->getRegion();
        $region[] = $addressDto->getArea();
        $region = array_filter($region);

        $city[] = $addressDto->getCity();
        $city[] = $addressDto->getSettlement();
        $city = array_filter($city);

        $house[] = $addressDto->getHouse();
        $house[] = $addressDto->getBlock();
        $house = array_filter($house);

        $address[] = $addressDto->getStreet();
        $address[] = implode(' ', $house);
        $address[] = $addressDto->getFlat();
        $address = array_filter($address);

        return [
            'postalCode' => $addressDto->getPostalCode(),
            'region' => implode(', ', $region),
            'city' => implode(', ', $city),
            'address' => implode(', ', $address),
        ];
    }
}
