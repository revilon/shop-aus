<?php

declare(strict_types=1);

namespace App\EmailManager;

use App\Entity\Shipping\ShippingMethod;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;
use Sylius\Bundle\AdminBundle\EmailManager\ShipmentEmailManagerInterface;
use Sylius\Component\Core\Model\ShipmentInterface;

final class ShipmentEmailManagerDecorate implements ShipmentEmailManagerInterface, LoggerAwareInterface
{
    use LoggerAwareTrait;

    /**
     * @var ShipmentEmailManagerInterface
     */
    private $shipmentEmailManager;

    /**
     * @param ShipmentEmailManagerInterface $shipmentEmailManager
     */
    public function __construct(ShipmentEmailManagerInterface $shipmentEmailManager)
    {
        $this->shipmentEmailManager = $shipmentEmailManager;
    }

    /**
     * {@inheritDoc}
     */
    public function sendConfirmationEmail(ShipmentInterface $shipment): void
    {
        if (!$shipment->getMethod()) {
            $this->logger->critical("Mail don't send to customer. Shipment haven't method shipping", [
                'shipment_id' => $shipment->getId(),
            ]);
        }

        if ($shipment->getMethod()->getCode() === ShippingMethod::PICKUP_CODE) {
            $this->logger->info('Shipping method pickup. Email send is skipped');

            return;
        }

        $this->shipmentEmailManager->sendConfirmationEmail($shipment);
    }
}
