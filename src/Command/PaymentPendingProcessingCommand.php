<?php

declare(strict_types=1);

namespace App\Command;

use App\Console\Lock\LockableInterface;
use App\Console\Lock\LockableTrait;
use App\UseCase\PaymentPendingProcessing\PaymentPendingProcessingHandler;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Exception\RuntimeException;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class PaymentPendingProcessingCommand extends Command implements LockableInterface
{
    use LockableTrait;

    protected static $defaultName = 'payment:pending_processing';

    /**
     * @var PaymentPendingProcessingHandler
     */
    private $handler;

    /**
     * @required
     *
     * @param PaymentPendingProcessingHandler $handler
     */
    public function dependencyInjection(
        PaymentPendingProcessingHandler $handler
    ): void {
        $this->handler = $handler;
    }

    /**
     * {@inheritDoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        if (!$this->lock()) {
            throw new RuntimeException('The command is already running in another process.');
        }

        $this->handler->handle();

        sleep(1);
    }
}
