<?php

declare(strict_types=1);

namespace App\HttpKernel\CacheClearer;

use Exception;
use FOS\JsRoutingBundle\Command\DumpCommand;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\NullOutput;
use Symfony\Component\HttpKernel\CacheClearer\CacheClearerInterface;
use Symfony\Component\HttpKernel\KernelInterface;

class FosJsRoutingCacheClearer implements CacheClearerInterface
{
    public const CACHE_FILE_NAME = 'fos_js_routes.json';

    /**
     * @var KernelInterface
     */
    private $kernel;

    /**
     * @var string
     */
    private $publicDir;

    /**
     * @param KernelInterface $kernel
     * @param string $publicDir
     */
    public function __construct(KernelInterface $kernel, string $publicDir)
    {
        $this->kernel = $kernel;
        $this->publicDir = $publicDir;
    }

    /**
     * {@inheritDoc}
     *
     * @throws Exception
     */
    public function clear($cacheDir): void
    {
        $application = new Application($this->kernel);
        $application->setAutoExit(false);
        $command = $application->find(DumpCommand::getDefaultName());

        $input = new ArrayInput([
            '--format' => 'json',
            '--target' => $this->publicDir . '/' . self::CACHE_FILE_NAME,
        ]);

        $command->run($input, new NullOutput());
    }
}
