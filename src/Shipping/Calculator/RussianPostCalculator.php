<?php

declare(strict_types=1);

namespace App\Shipping\Calculator;

use App\Component\RussianPost\Dto\DeliveryRateEntryDto;
use App\Component\RussianPost\Exception\RussianPostException;
use App\Component\RussianPost\RussianPost;
use App\Entity\Shipping\Shipment;
use App\Entity\Shipping\ShippingMethod;
use InvalidArgumentException;
use Sylius\Component\Shipping\Calculator\CalculatorInterface;
use Sylius\Component\Shipping\Model\ShipmentInterface;

final class RussianPostCalculator implements CalculatorInterface
{
    public const TYPE = ShippingMethod::RUSSIAN_POST_CODE;

    /**
     * @var RussianPost
     */
    private $russianPost;

    /**
     * @var string
     */
    private $russianPostPostcodeFrom;

    /**
     * @required
     *
     * @param RussianPost $russianPost
     * @param string $russianPostPostcodeFrom
     */
    public function dependencyInjection(RussianPost $russianPost, string $russianPostPostcodeFrom): void
    {
        $this->russianPost = $russianPost;
        $this->russianPostPostcodeFrom = $russianPostPostcodeFrom;
    }

    /**
     * @param ShipmentInterface|Shipment $subject
     * @param array $configuration
     * @return int
     */
    public function calculate(ShipmentInterface $subject, array $configuration): int
    {
        if (!$subject->getOrder() || !$subject->getOrder()->getShippingAddress()) {
            throw new InvalidArgumentException('Order fill not correct');
        }

        $heightTotal = $lengthTotal = $widthTotal = $weightTotal = 0;

        foreach ($subject->getShippables() as $shippable) {
            $heightTotal += (int)$shippable->getShippingHeight();
            $lengthTotal += (int)$shippable->getShippingDepth();
            $widthTotal += (int)$shippable->getShippingWidth();
            $weightTotal += (int)$shippable->getShippingWeight();
        }

        $costTotal = $subject->getOrder()->getItemsTotal() / 100;
        $postcodeTo = $subject->getOrder()->getShippingAddress()->getPostcode();

        try {
            $deliveryRateResultDto = $this->russianPost->getDeliveryRate(
                new DeliveryRateEntryDto(
                    $costTotal,
                    $heightTotal,
                    $lengthTotal,
                    $widthTotal,
                    $weightTotal,
                    $this->russianPostPostcodeFrom,
                    $postcodeTo
                )
            );
        } catch (RussianPostException $exception) {
            return 0;
        }

        return $deliveryRateResultDto->getTotalCost();
    }

    /**
     * {@inheritDoc}
     */
    public function getType(): string
    {
        return self::TYPE;
    }
}
