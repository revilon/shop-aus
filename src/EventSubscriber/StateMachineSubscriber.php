<?php

declare(strict_types=1);

namespace App\EventSubscriber;

use App\UseCase\Refund\RefundHandler;
use Exception;
use Psr\Log\LoggerInterface;
use SM\Event\SMEvents;
use SM\Event\TransitionEvent;
use Sylius\Component\Core\Model\PaymentInterface;
use Sylius\Component\Payment\PaymentTransitions;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class StateMachineSubscriber implements EventSubscriberInterface
{
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var RefundHandler
     */
    private $refundHandler;

    /**
     * @param LoggerInterface $logger
     * @param RefundHandler $refundHandler
     */
    public function __construct(
        LoggerInterface $logger,
        RefundHandler $refundHandler
    ) {
        $this->refundHandler = $refundHandler;
        $this->logger = $logger;
    }

    /**
     * {@inheritDoc}
     */
    public static function getSubscribedEvents(): array
    {
        return [
            SMEvents::PRE_TRANSITION => [
                'refundAction',
            ]
        ];
    }

    /**
     * @param TransitionEvent $transitionEvent
     */
    public function refundAction(TransitionEvent $transitionEvent): void
    {
        if ($transitionEvent->getTransition() !== PaymentTransitions::TRANSITION_REFUND) {
            return;
        }

        $payment = $transitionEvent->getStateMachine()->getObject();

        if (!$payment instanceof PaymentInterface) {
            return;
        }

        try {
            $this->refundHandler->handle($payment);
        } catch (Exception $exception) {
            $transitionEvent->setRejected(true);

            $this->logger->critical("Fail refund for payment id {$payment->getId()}: {$exception->getMessage()}");
        }
    }
}
