<?php

declare(strict_types=1);

namespace App\EventSubscriber;

use App\UseCase\AdminMailOrder\AdminMailOrderHandler;
use Sylius\Component\Core\Model\OrderInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\EventDispatcher\GenericEvent;
use Webmozart\Assert\Assert;

class OrderCompleteSubscriber implements EventSubscriberInterface
{
    /**
     * @var AdminMailOrderHandler
     */
    private $adminMailOrderHandler;

    /**
     * @param AdminMailOrderHandler $adminMailOrderHandler
     */
    public function __construct(AdminMailOrderHandler $adminMailOrderHandler)
    {
        $this->adminMailOrderHandler = $adminMailOrderHandler;
    }

    /**
     * {@inheritDoc}
     */
    public static function getSubscribedEvents(): array
    {
        return [
            'sylius.order.post_complete' => [
                'sendAdminMailOrderAction',
            ]
        ];
    }

    /**
     * @param GenericEvent $event
     */
    public function sendAdminMailOrderAction(GenericEvent $event): void
    {
        $order = $event->getSubject();
        Assert::isInstanceOf($order, OrderInterface::class);

        $this->adminMailOrderHandler->handle($order);
    }
}
