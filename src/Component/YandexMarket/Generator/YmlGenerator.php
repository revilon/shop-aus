<?php

declare(strict_types=1);

namespace App\Component\YandexMarket\Generator;

use App\Entity\Channel\Channel;
use App\Entity\Currency\Currency;
use App\Entity\Product\Product;
use App\Entity\Product\ProductAttribute;
use App\Entity\Product\ProductAttributeValue;
use App\Entity\Product\ProductImage;
use App\Entity\Product\ProductVariant;
use App\Entity\Taxonomy\Taxon;
use DOMDocument;
use DOMElement;
use Liip\ImagineBundle\Imagine\Cache\CacheManager;
use Psr\Log\LoggerInterface;
use Sylius\Component\Channel\Repository\ChannelRepositoryInterface;
use Sylius\Component\Core\Model\ChannelPricingInterface;
use Sylius\Component\Core\Repository\ProductRepositoryInterface;
use Sylius\Component\Inventory\Checker\AvailabilityChecker;
use Sylius\Component\Resource\Repository\RepositoryInterface;
use Sylius\Component\Taxonomy\Repository\TaxonRepositoryInterface;
use Symfony\Component\HttpFoundation\UrlHelper;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class YmlGenerator
{
    protected const XML_VERSION = '1.0';
    protected const XML_ENCODING = 'utf-8';

    public const RATE_CBRF = 'CBRF';
    public const RATE_NBU = 'NBU';
    public const RATE_NBK = 'NBK';
    public const RATE_CB = 'CB';

    public const DELIVERY_MAX_DAYS = '1-2';
    public const DELIVERY_MAX_PAY = '200';

    public const PICKUP_MAX_DAYS = '0';
    public const PICKUP_MAX_PAY = '0';
    public const PICKUP_ORDER_BEFORE = '20';

    /**
     * @var DOMDocument
     */
    protected $dom;

    /**
     * @var ChannelRepositoryInterface
     */
    private $channelRepository;

    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * @var RepositoryInterface
     */
    private $currencyRepository;

    /**
     * @var TaxonRepositoryInterface
     */
    private $taxonRepository;

    /**
     * @var ProductRepositoryInterface
     */
    private $productRepository;

    /**
     * @var RouterInterface
     */
    private $router;

    /**
     * @var UrlHelper
     */
    private $urlHelper;

    /**
     * @var CacheManager
     */
    private $imagineCacheManager;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var AvailabilityChecker
     */
    private $availabilityChecker;

    /**
     * @var int
     */
    private $minimumOrderAmountLocalRegion;

    /**
     * @param ChannelRepositoryInterface $channelRepository
     * @param TranslatorInterface $translator
     * @param RepositoryInterface $currencyRepository
     * @param TaxonRepositoryInterface $taxonRepository
     * @param ProductRepositoryInterface $productRepository
     * @param RouterInterface $router
     * @param UrlHelper $urlHelper
     * @param CacheManager $imagineCacheManager
     * @param LoggerInterface $logger
     * @param AvailabilityChecker $availabilityChecker
     * @param int $minimumOrderAmountLocalRegion
     */
    public function __construct(
        ChannelRepositoryInterface $channelRepository,
        TranslatorInterface $translator,
        RepositoryInterface $currencyRepository,
        TaxonRepositoryInterface $taxonRepository,
        ProductRepositoryInterface $productRepository,
        RouterInterface $router,
        UrlHelper $urlHelper,
        CacheManager $imagineCacheManager,
        LoggerInterface $logger,
        AvailabilityChecker $availabilityChecker,
        int $minimumOrderAmountLocalRegion
    ) {
        $this->channelRepository = $channelRepository;
        $this->translator = $translator;
        $this->currencyRepository = $currencyRepository;
        $this->taxonRepository = $taxonRepository;
        $this->productRepository = $productRepository;
        $this->router = $router;
        $this->urlHelper = $urlHelper;
        $this->imagineCacheManager = $imagineCacheManager;
        $this->logger = $logger;
        $this->availabilityChecker = $availabilityChecker;
        $this->minimumOrderAmountLocalRegion = $minimumOrderAmountLocalRegion;
    }

    /**
     * @return string
     */
    public function generate(): string
    {
        $this->dom = new DOMDocument(static::XML_VERSION, static::XML_ENCODING);

        $ymlCatalogElement = $this->dom->createElement('yml_catalog');
        $ymlCatalogElement->setAttribute('date', date('Y-m-d H:i:s'));

        $shopElement = $this->makeShopElement();

        $ymlCatalogElement->appendChild($shopElement);

        $this->dom->appendChild($ymlCatalogElement);

        return $this->dom->saveXML();
    }

    /**
     * @return DOMElement
     */
    protected function makeShopElement(): DOMElement
    {
        $shopElement = $this->dom->createElement('shop');

        $shopNameElement = $this->dom->createElement('name');
        $shopCompanyElement = $this->dom->createElement('company');
        $shopUrlElement = $this->dom->createElement('url');
        $currenciesElement = $this->makeCurrenciesElement();
        $categoriesElement = $this->makeCategoriesElement();
        $deliveryOptionsElement = $this->makeDeliveryOptionsElement();
        $pickupOptionsElement = $this->makePickupOptionsElement();
        $offersElement = $this->makeOffersElement();

        /** @var Channel $channel */
        $channel = $this->channelRepository->findOneByCode(Channel::DEFAULT_CHANNEL_CODE);
        $shopNameElement->nodeValue = $channel->getName();
        $shopCompanyElement->nodeValue = $this->translator->trans('sylius.ui.company');
        $shopUrlElement->nodeValue = 'https://' . $channel->getHostname();

        $shopElement->appendChild($shopNameElement);
        $shopElement->appendChild($shopCompanyElement);
        $shopElement->appendChild($shopUrlElement);
        $shopElement->appendChild($currenciesElement);
        $shopElement->appendChild($categoriesElement);
        $shopElement->appendChild($deliveryOptionsElement);
        $shopElement->appendChild($pickupOptionsElement);
        $shopElement->appendChild($offersElement);

        return $shopElement;
    }

    /**
     * @return DOMElement
     */
    protected function makeCurrenciesElement(): DOMElement
    {
        $currenciesElement = $this->dom->createElement('currencies');

        /** @var Currency $currency */
        foreach ($this->currencyRepository->findAll() as $currency) {
            $currencyElement = $this->dom->createElement('currency');
            $currencyElement->setAttribute('id', $currency->getCode());
            if ($currency->getCode() === 'RUB') {
                $currencyElement->setAttribute('rate', '1');
            } else {
                $currencyElement->setAttribute('rate', self::RATE_CBRF);
            }

            $currenciesElement->appendChild($currencyElement);
        }

        return $currenciesElement;
    }

    /**
     * @return DOMElement
     */
    public function makeCategoriesElement(): DOMElement
    {
        $categoriesElement = $this->dom->createElement('categories');

        /** @var Taxon $taxon */
        foreach ($this->taxonRepository->findAll() as $taxon) {
            if ($taxon->getCode() === Taxon::ROOT_TAXON_CODE) {
                continue;
            }

            $categoryElement = $this->dom->createElement('category');

            $categoryElement->setAttribute('id', (string)$taxon->getId());
            $categoryElement->nodeValue = $taxon->getName();

            if ($taxon->getParent() && $taxon->getParent()->getCode() !== Taxon::ROOT_TAXON_CODE) {
                $categoryElement->setAttribute('parentId', (string)$taxon->getParent()->getId());
            }

            $categoriesElement->appendChild($categoryElement);
        }

        return $categoriesElement;
    }

    /**
     * @return DOMElement
     */
    public function makeDeliveryOptionsElement(): DOMElement
    {
        $deliveryOptionsElement = $this->dom->createElement('delivery-options');

        $optionElement = $this->dom->createElement('option');
        $optionElement->setAttribute('cost', self::DELIVERY_MAX_PAY);
        $optionElement->setAttribute('days', self::DELIVERY_MAX_DAYS);

        $deliveryOptionsElement->appendChild($optionElement);

        return $deliveryOptionsElement;
    }

    /**
     * @return DOMElement
     */
    public function makePickupOptionsElement(): DOMElement
    {
        $pickupOptionsElement = $this->dom->createElement('pickup-options');

        $optionElement = $this->dom->createElement('option');
        $optionElement->setAttribute('cost', self::PICKUP_MAX_PAY);
        $optionElement->setAttribute('days', self::PICKUP_MAX_DAYS);
        $optionElement->setAttribute('order-before', self::PICKUP_ORDER_BEFORE);

        $pickupOptionsElement->appendChild($optionElement);

        return $pickupOptionsElement;
    }

    /**
     * @return DOMElement
     */
    public function makeOffersElement(): DOMElement
    {
        /** @var Channel $channel */
        $channel = $this->channelRepository->findOneByCode(Channel::DEFAULT_CHANNEL_CODE);

        $offersElement = $this->dom->createElement('offers');

        /** @var Product $product */
        foreach ($this->productRepository->findAll() as $product) {
            if (!$product->isEnabled()) {
                continue;
            }

            if (!$this->availabilityChecker->isStockAvailable($product->getVariants()->first())) {
                continue;
            }

            $offerElement = $this->dom->createElement('offer');
            $offerElement->setAttribute('id', (string)$product->getId());

            $nameElement = $this->dom->createElement('name', $product->getName());

            $url = $this->router->generate('sylius_shop_product_show', ['slug' => $product->getSlug()]);
            $url = $this->urlHelper->getAbsoluteUrl($url);
            $urlElement = $this->dom->createElement('url', $url);

            /** @var ProductVariant $variant */
            $variant = $product->getVariants()[0];
            /** @var ChannelPricingInterface $channelPricing */
            $channelPricing = $variant->getChannelPricingForChannel($channel);
            $price = (string)round($channelPricing->getPrice() / 100, 1);
            $priceElement = $this->dom->createElement('price', $price);

            $enableAutoDiscountsElement = $this->dom->createElement('enable_auto_discounts', 'true');

            $currencyIdElement = $this->dom->createElement('currencyId', $channel->getBaseCurrency()->getCode());

            /** @var Taxon $taxon */
            $taxon = $product->getTaxons()[0] ?? null;
            if (!$taxon) {
                continue;
            }
            $categoryIdElement = $this->dom->createElement('categoryId', (string)$taxon->getId());

            $pictureElementList = [];
            /** @var ProductImage $image */
            foreach ($product->getImages() as $image) {
                $pictureUrl = $this->imagineCacheManager->getBrowserPath(
                    $image->getPath(),
                    'sylius_shop_product_original'
                );
                $pictureElementList[] = $this->dom->createElement('picture', $pictureUrl);
            }

            $deliveryElement = $this->dom->createElement('delivery', 'true');

            $pickupElement = $this->dom->createElement('pickup', 'true');

            $descriptionElement = $this->dom->createElement('description', $product->getDescription() ?? '');

            $paramElementList = [];
            /** @var ProductAttributeValue $attribute */
            foreach ($product->getAttributes() as $attribute) {
                if ($attribute->getCode() === ProductAttribute::SECOND_HAND) {
                    continue;
                }

                $humanValueList = $attribute->getHumanValueList();

                foreach ($humanValueList as $humanValue) {
                    $param = $this->dom->createElement('param', (string)$humanValue);
                    $param->setAttribute('name', $attribute->getName());
                    $paramElementList[] = $param;
                }
            }

            $isSecondHand = $product->hasAttributeByCodeAndLocale(ProductAttribute::SECOND_HAND);
            $secondHandDescription = $product->getSecondHandDescription();

            if ((!$secondHandDescription && $isSecondHand) || ($secondHandDescription && !$isSecondHand)) {
                $this->logger->error(
                    'Product have second hand description and have not attribute second hand or vice versa',
                    [
                        'product_id' => $product->getId(),
                    ]
                );

                continue;
            }

            if ($isSecondHand && $secondHandDescription) {
                $reason = $this->dom->createElement('reason', $secondHandDescription);

                $condition = $this->dom->createElement('condition');
                $condition->setAttribute('type', 'used');

                $condition->appendChild($reason);
            }

            $weight = (string)round($variant->getWeight() / 1000, 3);
            $weightElement = $this->dom->createElement('weight', $weight);

            $height = (string)round($variant->getHeight() / 10, 3);
            $width = (string)round($variant->getWidth() / 10, 3);
            $depth = (string)round($variant->getDepth() / 10, 3);
            $dimensions = "$depth/$width/$height";
            $dimensionsElement = $this->dom->createElement('dimensions', $dimensions);

            $offerElement->appendChild($nameElement);
            $offerElement->appendChild($urlElement);
            $offerElement->appendChild($priceElement);
            $offerElement->appendChild($enableAutoDiscountsElement);
            $offerElement->appendChild($currencyIdElement);
            $offerElement->appendChild($categoryIdElement);
            array_map([$offerElement, 'appendChild'], $pictureElementList);
            $offerElement->appendChild($deliveryElement);
            $offerElement->appendChild($pickupElement);
            $offerElement->appendChild($descriptionElement);
            array_map([$offerElement, 'appendChild'], $paramElementList);
            if ($isSecondHand && $secondHandDescription && isset($condition)) {
                $offerElement->appendChild($condition);
            }
            $offerElement->appendChild($weightElement);
            $offerElement->appendChild($dimensionsElement);

            $offersElement->appendChild($offerElement);
        }

        return $offersElement;
    }
}
