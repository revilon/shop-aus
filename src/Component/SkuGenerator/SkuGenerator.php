<?php

declare(strict_types=1);

namespace App\Component\SkuGenerator;

use Exception;

use function random_int;

class SkuGenerator
{
    protected const SKU_LENGTH = 6;

    /**
     * @var string
     */
    private $uriSafeAlphabet;

    public function __construct()
    {
        $this->uriSafeAlphabet =
            implode(range(0, 9)) .
            implode(range('A', 'Z'))
        ;
    }

    /**
     * @param array $existSkuList
     *
     * @return string
     *
     * @throws Exception
     */
    public function generateSku(array $existSkuList = []): string
    {
        do {
            $sku = $this->generateStringOfLength(static::SKU_LENGTH, $this->uriSafeAlphabet);
        } while (isset($existSkuList[$sku]));

        return $sku;
    }

    /**
     * @param int $length
     * @param string $alphabet
     *
     * @return string
     *
     * @throws Exception
     */
    protected function generateStringOfLength(int $length, string $alphabet): string
    {
        $alphabetMaxIndex = strlen($alphabet) - 1;
        $randomString = '';

        for ($i = 0; $i < $length; ++$i) {
            $index = random_int(0, $alphabetMaxIndex);
            $randomString .= $alphabet[$index];
        }

        return $randomString;
    }
}
