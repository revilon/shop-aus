<?php

declare(strict_types=1);

namespace App\Component\PaymentGetawayYandex\Payum;

use App\Component\PaymentGetawayYandex\Payum\Action\ObtainTokenAction;
use App\Component\PaymentGetawayYandex\Payum\Action\RefundAction;
use App\Component\PaymentGetawayYandex\Payum\Action\StatusAction;
use Payum\Core\Bridge\Spl\ArrayObject;
use Payum\Core\GatewayFactory;

class YandexPaymentGatewayFactory extends GatewayFactory
{
    /**
     * {@inheritDoc}
     */
    protected function populateConfig(ArrayObject $config): void
    {
        $config->defaults([
            'payum.factory_name' => 'yandex_payment',
            'payum.factory_title' => 'Yandex Payment',

            'payum.template.obtain_token' => 'Shop/checkout_pay.html.twig',

            'payum.action.obtain_token' => static function (ArrayObject $config) {
                return new ObtainTokenAction($config['payum.template.obtain_token']);
            },
            'payum.action.status' => static function (ArrayObject $config) {
                return new StatusAction($config['payum.http_client']);
            },
            'payum.action.refund' => static function (ArrayObject $config) {
                return new RefundAction($config['payum.http_client']);
            },
        ]);

        $config['payum.api'] = static function (ArrayObject $config) {
            return new YandexApi($config['shop_id'], $config['secret_key']);
        };
    }
}
