<?php

declare(strict_types=1);

namespace App\Component\PaymentGetawayYandex\Payum\Action;

use App\Component\PaymentGetawayYandex\Payum\Request\Api\ObtainToken;
use App\Component\PaymentGetawayYandex\Payum\YandexApi;
use GuzzleHttp\Psr7\Request;
use Payum\Core\Action\ActionInterface;
use Payum\Core\ApiAwareInterface;
use Payum\Core\ApiAwareTrait;
use Payum\Core\Exception\RequestNotSupportedException;
use Payum\Core\GatewayAwareInterface;
use Payum\Core\GatewayAwareTrait;
use Payum\Core\HttpClientInterface;
use Payum\Core\Request\Capture;
use Sylius\Component\Core\Model\PaymentInterface;

use function GuzzleHttp\json_decode;
use function GuzzleHttp\json_encode;

final class CaptureAction implements ActionInterface, ApiAwareInterface, GatewayAwareInterface
{
    use ApiAwareTrait;
    use GatewayAwareTrait;

    private const CREATE_PAYMENT_URL = 'https://payment.yandex.net/api/v3/payments';

    /**
     * @var HttpClientInterface
     */
    protected $client;

    /**
     * @param HttpClientInterface $client
     */
    public function __construct(HttpClientInterface $client)
    {
        $this->client = $client;
        $this->apiClass = YandexApi::class;
    }

    /**
     * @param Capture $request
     */
    public function execute($request): void
    {
        RequestNotSupportedException::assertSupports($this, $request);

        /** @var PaymentInterface $payment */
        $payment = $request->getModel();

        if (isset($payment->getDetails()['payment_id'])) {
            return;
        }

        $paymentData = $this->initPayment($payment);

        $payment->setDetails([
            'payment_id' => $paymentData['id'],
            'confirmation_token' => $paymentData['confirmation']['confirmation_token'],
        ]);

        $obtainToken = new ObtainToken($request->getToken());
        $obtainToken->setModel($payment);

        $this->gateway->execute($obtainToken);
    }

    /**
     * {@inheritDoc}
     */
    public function supports($request): bool
    {
        return $request instanceof Capture && $request->getModel() instanceof PaymentInterface;
    }

    /**
     * @param PaymentInterface $payment
     *
     * @return array
     */
    protected function initPayment(PaymentInterface $payment): array
    {
        $auth = base64_encode("{$this->api->getShopId()}:{$this->api->getSecretKey()}");

        $request = new Request(
            'POST',
            self::CREATE_PAYMENT_URL,
            [
                'Idempotence-Key' => uniqid('', true),
                'Content-Type' => 'application/json',
                'Authorization' => "Basic $auth",
            ],
            json_encode([
                'amount' => [
                    'value' => (string)round($payment->getAmount() / 100, 2),
                    'currency' => $payment->getCurrencyCode()
                ],
                'confirmation' => [
                    'type' => 'embedded',
                ],
                'capture' => true,
                'description' => "Заказ №{$payment->getOrder()->getNumber()}",
                'metadata' => [
                    'order_id' => $payment->getOrder()->getId(),
                ]
            ])
        );

        $response = $this->client->send($request);

        return json_decode($response->getBody(), true);
    }
}
