<?php

declare(strict_types=1);

namespace App\Component\PaymentGetawayYandex\Payum\Action;

use App\Component\PaymentGetawayYandex\Payum\YandexApi;
use ArrayAccess;
use GuzzleHttp\Psr7\Request;
use Payum\Core\Action\ActionInterface;
use Payum\Core\ApiAwareInterface;
use Payum\Core\ApiAwareTrait;
use Payum\Core\Exception\RequestNotSupportedException;
use Payum\Core\HttpClientInterface;
use Payum\Core\Request\Refund;
use Sylius\Component\Core\Model\PaymentInterface;

use function GuzzleHttp\json_decode;
use function GuzzleHttp\json_encode;

final class RefundAction implements ActionInterface, ApiAwareInterface
{
    use ApiAwareTrait;

    private const REFUND_PAYMENT_URL = 'https://payment.yandex.net/api/v3/refunds';

    /**
     * @var HttpClientInterface
     */
    private $client;

    /**
     * @param HttpClientInterface $client
     */
    public function __construct(HttpClientInterface $client)
    {
        $this->client = $client;
        $this->apiClass = YandexApi::class;
    }

    /**
     * {@inheritDoc}
     */
    public function execute($request)
    {
        /** @var $request Refund */
        RequestNotSupportedException::assertSupports($this, $request);

        /** @var PaymentInterface $payment */
        $payment = $request->getFirstModel();

        $this->initRefund($payment);
    }

    /**
     * {@inheritDoc}
     */
    public function supports($request)
    {
        return $request instanceof Refund && $request->getModel() instanceof ArrayAccess;
    }

    /**
     * @param PaymentInterface $payment
     *
     * @return array
     */
    protected function initRefund(PaymentInterface $payment): array
    {
        $auth = base64_encode("{$this->api->getShopId()}:{$this->api->getSecretKey()}");

        $request = new Request(
            'POST',
            static::REFUND_PAYMENT_URL,
            [
                'Idempotence-Key' => uniqid('', true),
                'Content-Type' => 'application/json',
                'Authorization' => "Basic $auth",
            ],
            json_encode([
                'amount' => [
                    'value' => (string)round($payment->getAmount() / 100, 2),
                    'currency' => $payment->getCurrencyCode(),
                ],
                'payment_id' => $payment->getDetails()['payment_id'],
            ])
        );

        $response = $this->client->send($request);

        return json_decode($response->getBody(), true);
    }
}
