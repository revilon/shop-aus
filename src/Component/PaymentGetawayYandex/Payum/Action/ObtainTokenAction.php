<?php

declare(strict_types=1);

namespace App\Component\PaymentGetawayYandex\Payum\Action;

use App\Component\PaymentGetawayYandex\Payum\Request\Api\ObtainToken;
use App\Component\PaymentGetawayYandex\Payum\YandexApi;
use ArrayAccess;
use Payum\Core\Action\ActionInterface;
use Payum\Core\ApiAwareInterface;
use Payum\Core\ApiAwareTrait;
use Payum\Core\Exception\RequestNotSupportedException;
use Payum\Core\GatewayAwareInterface;
use Payum\Core\GatewayAwareTrait;
use Payum\Core\Reply\HttpResponse;
use Payum\Core\Request\RenderTemplate;
use Sylius\Component\Core\Model\PaymentInterface;

class ObtainTokenAction implements ActionInterface, GatewayAwareInterface, ApiAwareInterface
{
    use ApiAwareTrait;
    use GatewayAwareTrait;

    /**
     * @var string
     */
    protected $templateName;

    /**
     * @param string $templateName
     */
    public function __construct($templateName)
    {
        $this->templateName = $templateName;
        $this->apiClass = YandexApi::class;
    }

    /**
     * {@inheritDoc}
     */
    public function execute($request)
    {
        /** @var $request ObtainToken */
        RequestNotSupportedException::assertSupports($this, $request);

        /** @var PaymentInterface $payment */
        $payment = $request->getFirstModel();

        $renderTemplate = new RenderTemplate($this->templateName, array(
            'confirmation_token' => $payment->getDetails()['confirmation_token'],
            'return_url' => $request->getToken()->getTargetUrl(),
        ));
        $this->gateway->execute($renderTemplate);

        throw new HttpResponse($renderTemplate->getResult());
    }

    /**
     * {@inheritDoc}
     */
    public function supports($request)
    {
        return $request instanceof ObtainToken && $request->getModel() instanceof ArrayAccess;
    }
}
