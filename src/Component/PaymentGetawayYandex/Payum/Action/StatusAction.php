<?php

declare(strict_types=1);

namespace App\Component\PaymentGetawayYandex\Payum\Action;

use App\Component\PaymentGetawayYandex\Payum\YandexApi;
use GuzzleHttp\Psr7\Request;
use Payum\Core\Action\ActionInterface;
use Payum\Core\ApiAwareInterface;
use Payum\Core\ApiAwareTrait;
use Payum\Core\Exception\RequestNotSupportedException;
use Payum\Core\HttpClientInterface;
use Payum\Core\Request\GetStatusInterface;
use Sylius\Bundle\PayumBundle\Request\GetStatus;
use Sylius\Component\Core\Model\PaymentInterface;

use function GuzzleHttp\json_decode;

final class StatusAction implements ActionInterface, ApiAwareInterface
{
    use ApiAwareTrait;

    public const PAYMENT_STATUS_PENDING = 'pending';
    public const PAYMENT_STATUS_SUCCEEDED = 'succeeded';
    public const PAYMENT_STATUS_WAITING_FOR_CAPTURE = 'waiting_for_capture';
    public const PAYMENT_STATUS_CANCELED = 'canceled';

    protected const CHECK_PAYMENT_URL_PATTERN = 'https://payment.yandex.net/api/v3/payments/%s';

    /**
     * @var HttpClientInterface
     */
    private $client;

    /**
     * @param HttpClientInterface $client
     */
    public function __construct(HttpClientInterface $client)
    {
        $this->client = $client;
        $this->apiClass = YandexApi::class;
    }

    /**
     * @param GetStatus $request
     */
    public function execute($request): void
    {
        RequestNotSupportedException::assertSupports($this, $request);

        /** @var PaymentInterface $payment */
        $payment = $request->getFirstModel();

        $paymentData = $this->checkPayment($payment);

        if ($paymentData['status'] === static::PAYMENT_STATUS_SUCCEEDED) {
            $request->markCaptured();

            return;
        }

        if ($paymentData['status'] === static::PAYMENT_STATUS_PENDING) {
            $request->markPending();

            return;
        }

        if ($paymentData['status'] === static::PAYMENT_STATUS_CANCELED) {
            $request->markCanceled();

            return;
        }

        if ($paymentData['status'] === static::PAYMENT_STATUS_WAITING_FOR_CAPTURE) {
            $request->markPending();

            return;
        }
    }

    public function supports($request): bool
    {
        return $request instanceof GetStatusInterface && $request->getFirstModel() instanceof PaymentInterface;
    }

    /**
     * @param PaymentInterface $payment
     *
     * @return array
     */
    protected function checkPayment(PaymentInterface $payment): array
    {
        $auth = base64_encode("{$this->api->getShopId()}:{$this->api->getSecretKey()}");

        $request = new Request(
            'GET',
            sprintf(static::CHECK_PAYMENT_URL_PATTERN, $payment->getDetails()['payment_id']),
            ['Authorization' => "Basic $auth"]
        );

        $response = $this->client->send($request);

        return json_decode($response->getBody(), true);
    }
}
