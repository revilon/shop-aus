<?php

declare(strict_types=1);

namespace App\Component\PaymentGetawayYandex\Payum;

final class YandexApi
{
    /**
     * @var string
     */
    private $shopId;

    /**
     * @var string
     */
    private $secretKey;

    /**
     * @param string $shopId
     * @param string $secretKey
     */
    public function __construct(string $shopId, string $secretKey)
    {
        $this->shopId = $shopId;
        $this->secretKey = $secretKey;
    }

    /**
     * @return string
     */
    public function getShopId(): string
    {
        return $this->shopId;
    }

    /**
     * @return string
     */
    public function getSecretKey(): string
    {
        return $this->secretKey;
    }
}
