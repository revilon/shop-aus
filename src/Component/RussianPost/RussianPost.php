<?php

declare(strict_types=1);

namespace App\Component\RussianPost;

use App\Component\RussianPost\Adapter\AdapterInterface;
use App\Component\RussianPost\Dto\DeliveryRateEntryDto;
use App\Component\RussianPost\Dto\DeliveryRateResultDto;
use App\Component\RussianPost\Exception\RussianPostDeliveryRateException;
use Exception;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ServiceLocator;

class RussianPost
{
    /**
     * @var AdapterInterface
     */
    private $adapter;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @param ServiceLocator $adapterList
     * @param string $adapterKey
     * @param LoggerInterface $logger
     */
    public function __construct(
        ServiceLocator $adapterList,
        string $adapterKey,
        LoggerInterface $logger
    ) {
        $this->adapter = $adapterList->get($adapterKey);
        $this->logger = $logger;
    }

    /**
     * @param DeliveryRateEntryDto $deliveryRateEntryDto
     *
     * @return DeliveryRateResultDto
     */
    public function getDeliveryRate(DeliveryRateEntryDto $deliveryRateEntryDto): DeliveryRateResultDto
    {
        try {
            $requestData = $deliveryRateEntryDto->dehydrate();
            $responseData = $this->adapter->getDeliveryRate($requestData);

            $resultDtoClassName = $deliveryRateEntryDto::getResultDtoClassName();

            return new $resultDtoClassName($responseData);
        } catch (Exception $exception) {
            $message = "Error russian post delivery rate with message: '{$exception->getMessage()}'";
            $this->logger->critical($message, explode(PHP_EOL, $exception->getTraceAsString()));

            throw new RussianPostDeliveryRateException($message);
        }
    }
}
