<?php

declare(strict_types=1);

namespace App\Component\RussianPost\Dto;

class DeliveryRateEntryDto implements EntryDtoInterface
{
    /**
     * @var int
     */
    private $declaredCost;

    /**
     * @var int
     */
    private $dimensionHeight;

    /**
     * @var int
     */
    private $dimensionLength;

    /**
     * @var int
     */
    private $dimensionWidth;

    /**
     * @var int
     */
    private $weight;

    /**
     * @var string
     */
    private $indexFrom;

    /**
     * @var string
     */
    private $indexTo;

    /**
     * @param int $declaredCost
     * @param int $dimensionHeight
     * @param int $dimensionLength
     * @param int $dimensionWidth
     * @param int $weight
     * @param string $indexFrom
     * @param string $indexTo
     */
    public function __construct(
        int $declaredCost,
        int $dimensionHeight,
        int $dimensionLength,
        int $dimensionWidth,
        int $weight,
        string $indexFrom,
        string $indexTo
    ) {
        $this->declaredCost = $declaredCost;
        $this->dimensionHeight = $dimensionHeight;
        $this->dimensionLength = $dimensionLength;
        $this->dimensionWidth = $dimensionWidth;
        $this->weight = $weight;
        $this->indexFrom = $indexFrom;
        $this->indexTo = $indexTo;
    }

    /**
     * {@inheritDoc}
     */
    public function dehydrate(): array
    {
        return [
            'declared-value' => $this->declaredCost,
            'dimension' => [
                'height' => $this->dimensionHeight,
                'length' => $this->dimensionLength,
                'width' => $this->dimensionWidth,
            ],
            'mass' => $this->weight,
            'index-from' => $this->indexFrom,
            'index-to' => $this->indexTo,
            'inventory' => false,
            'mail-category' => 'WITH_DECLARED_VALUE',
            'mail-type' => 'PARCEL_CLASS_1',
        ];
    }

    /**
     * {@inheritDoc}
     */
    public static function getResultDtoClassName(): ?string
    {
        return DeliveryRateResultDto::class;
    }
}
