<?php

declare(strict_types=1);

namespace App\Component\RussianPost\Dto;

interface EntryDtoInterface
{
    /**
     * @return array
     */
    public function dehydrate(): array;

    /**
     * @return string|null
     */
    public static function getResultDtoClassName(): ?string;
}
