<?php

declare(strict_types=1);

namespace App\Component\RussianPost\Dto;

class DeliveryRateResultDto
{
    /**
     * @var int
     */
    private $totalCost;

    /**
     * @var int
     */
    private $deliveryDayMax;

    /**
     * @var int
     */
    private $deliveryDayMin;

    /**
     * @param array $responseData
     */
    public function __construct(array $responseData)
    {
        $this->totalCost = $responseData['total-rate'] + $responseData['total-vat'];
        $this->deliveryDayMax = $responseData['delivery-time']['max-days'];
        $this->deliveryDayMin = $responseData['delivery-time']['min-days'] ?? $this->deliveryDayMax;
    }

    /**
     * @return int
     */
    public function getTotalCost(): int
    {
        return $this->totalCost;
    }

    /**
     * @return int
     */
    public function getDeliveryDayMax(): int
    {
        return $this->deliveryDayMax;
    }

    /**
     * @return int
     */
    public function getDeliveryDayMin(): int
    {
        return $this->deliveryDayMin;
    }
}
