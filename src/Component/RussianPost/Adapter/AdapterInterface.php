<?php

declare(strict_types=1);

namespace App\Component\RussianPost\Adapter;

interface AdapterInterface
{
    public const TAG = 'russian_post.adapter';

    public const DELIVERY_RATE_ENDPOINT = 'https://otpravka-api.pochta.ru/1.0/tariff';

    /**
     * @param array $requestData
     *
     * @return array
     */
    public function getDeliveryRate(array $requestData): array;

    /**
     * @return string
     */
    public static function getAdapterKey(): string;
}
