<?php

declare(strict_types=1);

namespace App\Component\RussianPost\Adapter;

use App\Component\RussianPost\Exception\RussianPostException;
use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;
use Psr\Log\LoggerInterface;

use function GuzzleHttp\json_decode;
use function GuzzleHttp\json_encode;

final class RestAdapter implements AdapterInterface
{
    public const KEY = 'rest';

    /**
     * @var Client
     */
    private $client;

    /**
     * @var string
     */
    private $russianPostAuthorizationToken;

    /**
     * @var string
     */
    private $russianPostAuthorizationUser;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @param string $russianPostAuthorizationToken
     * @param string $russianPostAuthorizationUser
     * @param Client $client
     * @param LoggerInterface $logger
     */
    public function __construct(
        string $russianPostAuthorizationToken,
        string $russianPostAuthorizationUser,
        Client $client,
        LoggerInterface $logger
    ) {
        $this->russianPostAuthorizationToken = $russianPostAuthorizationToken;
        $this->russianPostAuthorizationUser = $russianPostAuthorizationUser;
        $this->client = $client;
        $this->logger = $logger;
    }

    /**
     * {@inheritDoc}
     */
    public function getDeliveryRate(array $requestData): array
    {
        $this->logger->info('Russian post request delivery rate', $requestData);

        $response = $this->client->post(static::DELIVERY_RATE_ENDPOINT, [
            RequestOptions::HTTP_ERRORS => false,
            RequestOptions::HEADERS => [
                'Authorization' => 'AccessToken ' . $this->russianPostAuthorizationToken,
                'X-User-Authorization' => 'Basic ' . $this->russianPostAuthorizationUser,
                'Content-Type' => 'application/json;charset=UTF-8',
            ],
            RequestOptions::BODY => json_encode($requestData),
        ]);

        $responseString = $response->getBody()->getContents();

        $this->logger->info('Russian post response delivery rate', [
            'headerList' => $response->getHeaders(),
            'statusCode' => $response->getStatusCode(),
            'response' => $responseString,
        ]);

        if ($response->getStatusCode() !== 200) {
            throw new RussianPostException(sprintf('Adapter "%s" have wrong response code', self::KEY));
        }

        return json_decode($responseString, true);
    }

    /**
     * {@inheritDoc}
     */
    public static function getAdapterKey(): string
    {
        return self::KEY;
    }
}
