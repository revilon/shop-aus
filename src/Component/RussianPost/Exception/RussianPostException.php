<?php

declare(strict_types=1);

namespace App\Component\RussianPost\Exception;

use RuntimeException;

class RussianPostException extends RuntimeException
{
}
