<?php

declare(strict_types=1);

namespace App\Component\RussianPost\Exception;

class RussianPostDeliveryRateException extends RussianPostException
{
}
