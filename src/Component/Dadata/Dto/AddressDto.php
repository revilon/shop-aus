<?php

declare(strict_types=1);

namespace App\Component\Dadata\Dto;

class AddressDto
{
    /**
     * @var array
     */
    private $addressInfoRaw;

    /**
     * @var string
     */
    private $addressInline;

    /**
     * @var string|null
     */
    private $postalCode;

    /**
     * @var string|null
     */
    private $region;

    /**
     * @var string|null
     */
    private $city;

    /**
     * @var string|null
     */
    private $street;

    /**
     * @var string|null
     */
    private $house;

    /**
     * @var string|null
     */
    private $flat;

    /**
     * @var string|null
     */
    private $area;

    /**
     * @var string|null
     */
    private $settlement;

    /**
     * @var string|null
     */
    private $block;

    /**
     * @param array $addressInfoRaw
     */
    public function __construct(array $addressInfoRaw)
    {
        $this->addressInfoRaw = $addressInfoRaw;

        $this->addressInline = $addressInfoRaw['value'];
        $data = $addressInfoRaw['data'];

        $this->postalCode = $data['postal_code'];
        $this->region = $data['region_with_type'];
        $this->area = $data['area_with_type'];
        $this->settlement = $data['settlement_with_type'];
        $this->city = $data['city_with_type'];
        $this->street = $data['street_with_type'];
        $this->house = trim("$data[house_type_full] $data[house]") ?: null;
        $this->block = trim("$data[block_type_full] $data[block]") ?: null;
        $this->flat = trim("$data[flat_type_full] $data[flat]") ?: null;
    }

    /**
     * @return string
     */
    public function getAddressInline(): string
    {
        return $this->addressInline;
    }

    /**
     * @return array
     */
    public function getAddressInfoRaw(): array
    {
        return $this->addressInfoRaw;
    }

    /**
     * @return string|null
     */
    public function getPostalCode(): ?string
    {
        return $this->postalCode;
    }

    /**
     * @return string|null
     */
    public function getRegion(): ?string
    {
        return $this->region;
    }

    /**
     * @return string|null
     */
    public function getCity(): ?string
    {
        return $this->city;
    }

    /**
     * @return string|null
     */
    public function getStreet(): ?string
    {
        return $this->street;
    }

    /**
     * @return string|null
     */
    public function getHouse(): ?string
    {
        return $this->house;
    }

    /**
     * @return string|null
     */
    public function getFlat(): ?string
    {
        return $this->flat;
    }

    /**
     * @return string|null
     */
    public function getArea(): ?string
    {
        return $this->area;
    }

    /**
     * @return string|null
     */
    public function getSettlement(): ?string
    {
        return $this->settlement;
    }

    /**
     * @return string|null
     */
    public function getBlock(): ?string
    {
        return $this->block;
    }
}
