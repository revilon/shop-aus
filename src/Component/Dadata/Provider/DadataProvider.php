<?php

declare(strict_types=1);

namespace App\Component\Dadata\Provider;

use App\Component\Dadata\Dto\AddressDto;
use App\Component\Dadata\Dto\FullNameDto;
use App\Component\Dadata\Exception\DadataException;
use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;
use Symfony\Component\HttpFoundation\Response;

use function GuzzleHttp\json_decode;
use function GuzzleHttp\json_encode;

class DadataProvider
{
    private const URL_SUGGEST_FULL_NAME = 'https://suggestions.dadata.ru/suggestions/api/4_1/rs/suggest/fio';
    private const URL_SUGGEST_ADDRESS = 'https://suggestions.dadata.ru/suggestions/api/4_1/rs/suggest/address';

    /**
     * @var Client
     */
    private $client;

    /**
     * @var string
     */
    private $apiKey;

    /**
     * @var string
     */
    private $apiSecret;

    /**
     * @param Client $client
     * @param string $apiKey
     * @param string $apiSecret
     */
    public function __construct(Client $client, string $apiKey, string $apiSecret)
    {
        $this->client = $client;
        $this->apiKey = $apiKey;
        $this->apiSecret = $apiSecret;
    }

    /**
     * @param string $query
     *
     * @return FullNameDto[]
     *
     * @throws DadataException
     */
    public function getFullNameDtoList(string $query): array
    {
        $response = $this->client->post(self::URL_SUGGEST_FULL_NAME, [
            RequestOptions::HEADERS => [
                'Content-Type' => 'application/json',
                'Accept' => 'application/json',
                'Authorization' => "Token {$this->apiKey}" ,
            ],
            RequestOptions::BODY => json_encode([
                'query' => $query,
            ]),
            RequestOptions::HTTP_ERRORS => false,
        ]);

        if ($response->getStatusCode() !== Response::HTTP_OK) {
            throw new DadataException(sprintf(
                'Dadata return %s code. Response: "%s"',
                $response->getStatusCode(),
                $response->getBody()->getContents()
            ));
        }

        $response = json_decode($response->getBody()->getContents(), true);

        $resultList = [];

        foreach ($response['suggestions'] as $suggestion) {
            $resultList[] = new FullNameDto(
                $suggestion['value'],
                $suggestion['data']['surname'],
                $suggestion['data']['name'],
                $suggestion['data']['patronymic'],
                $suggestion['data']['gender']
            );
        }

        return $resultList;
    }

    /**
     * @param string $searchText
     *
     * @return AddressDto[]
     *
     * @throws DadataException
     */
    public function getAddressDtoList(string $searchText): array
    {
        $response = $this->client->post(self::URL_SUGGEST_ADDRESS, [
            RequestOptions::HEADERS => [
                'Content-Type' => 'application/json',
                'Accept' => 'application/json',
                'Authorization' => "Token {$this->apiKey}" ,
            ],
            RequestOptions::BODY => json_encode([
                'query' => $searchText,
            ]),
            RequestOptions::HTTP_ERRORS => false,
        ]);

        if ($response->getStatusCode() !== Response::HTTP_OK) {
            throw new DadataException(sprintf(
                'Dadata return %s code. Response: "%s"',
                $response->getStatusCode(),
                $response->getBody()->getContents()
            ));
        }

        $response = json_decode($response->getBody()->getContents(), true);

        $resultList = [];

        foreach ($response['suggestions'] as $suggestion) {
            $resultList[] = new AddressDto($suggestion);
        }

        return $resultList;
    }
}
