<?php

declare(strict_types=1);

namespace App\Enum;

use Sylius\Bundle\CoreBundle\Mailer\Emails as CoreEmails;
use Sylius\Bundle\UserBundle\Mailer\Emails as UserEmails;

class EmailVariantEnum extends AbstractEnum
{
    public const CONTACT_REQUEST = CoreEmails::CONTACT_REQUEST;
    public const ORDER_CONFIRMATION = CoreEmails::ORDER_CONFIRMATION;
    public const SHIPMENT_CONFIRMATION = CoreEmails::SHIPMENT_CONFIRMATION;
    public const USER_REGISTRATION = CoreEmails::USER_REGISTRATION;
    public const RESET_PASSWORD_TOKEN = UserEmails::RESET_PASSWORD_TOKEN;
    public const RESET_PASSWORD_PIN = UserEmails::RESET_PASSWORD_PIN;
    public const EMAIL_VERIFICATION_TOKEN = UserEmails::EMAIL_VERIFICATION_TOKEN;
    public const ADMIN_ORDER = 'admin_order';
}
