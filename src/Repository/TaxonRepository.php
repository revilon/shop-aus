<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Taxonomy\Taxon;
use Sylius\Bundle\TaxonomyBundle\Doctrine\ORM\TaxonRepository as SyliusTaxonRepository;

class TaxonRepository extends SyliusTaxonRepository
{
    /**
     * @param string $code
     *
     * @return Taxon
     */
    public function getTaxonByCode(string $code): Taxon
    {
        /** @var Taxon $taxon */
        $taxon = $this->findOneBy(['code' => $code]);

        return $taxon;
    }
}
