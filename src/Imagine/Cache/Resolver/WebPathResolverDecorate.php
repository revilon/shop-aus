<?php

declare(strict_types=1);

namespace App\Imagine\Cache\Resolver;

use Liip\ImagineBundle\Imagine\Cache\Resolver\WebPathResolver;

class WebPathResolverDecorate extends WebPathResolver
{
    /**
     * {@inheritDoc}
     */
    protected function getBaseUrl()
    {
        $url = parent::getBaseUrl();

        if (strpos($url, 'http://') === 0) {
            $url = str_replace('http://', 'https://', $url);
        }

        return $url;
    }
}
