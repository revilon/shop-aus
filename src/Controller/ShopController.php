<?php

declare(strict_types=1);

namespace App\Controller;

use App\UseCase\FetchWidgetCart\FetchWidgetCartHandler;
use App\UseCase\LatestProduct\LatestProductHandler;
use App\UseCase\TaxonList\TaxonListHandler;
use Doctrine\DBAL\DBALException;
use Exception;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\RouterInterface;

/**
 * @Route(
 *     "/{_locale}",
 *     requirements={"_locale": "^[A-Za-z]{2,4}(_([A-Za-z]{4}|[0-9]{3}))?(_([A-Za-z]{2}|[0-9]{3}))?$"},
 *     options={"expose": true}
 * )
 */
class ShopController extends AbstractController implements LoggerAwareInterface
{
    use LoggerAwareTrait;

    /**
     * @Route("/", methods={"GET"})
     *
     * @param Request $request
     * @param LatestProductHandler $latestProductHandler
     * @param TaxonListHandler $taxonListHandler
     * @param RouterInterface $router
     * @param FetchWidgetCartHandler $fetchCartHandler
     *
     * @return Response
     *
     * @throws DBALException
     */
    public function homepage(
        Request $request,
        LatestProductHandler $latestProductHandler,
        TaxonListHandler $taxonListHandler,
        RouterInterface $router,
        FetchWidgetCartHandler $fetchCartHandler
    ): Response {
        $latestProductList = [];
        $taxonList = [];

        try {
            $latestProductList = $latestProductHandler->handle(8);
        } catch (Exception $exception) {
            $this->logger->error("Can't get latest product. {$exception->getMessage()}");
        }

        try {
            $taxonList = $taxonListHandler->handle();
        } catch (Exception $exception) {
            $this->logger->error("Can't get taxon list. {$exception->getMessage()}");
        }

        $rootTaxonUrl = $router->generate('sylius_shop_product_index', [
            'slug' => $taxonList[0]['slug'],
            '_locale' => $request->attributes->get('_locale'),
        ]);

        $cart = $fetchCartHandler->handle($request);

        return $this->render('Shop/Homepage/index.html.twig', [
            'latest_product_list' => $latestProductList,
            'taxon_list' => $taxonList[0]['children'],
            'root_taxon_url' => $rootTaxonUrl,
            'cart' => $cart,
        ]);
    }
}
