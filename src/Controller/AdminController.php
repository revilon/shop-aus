<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Product\ProductAttribute;
use App\Entity\Taxonomy\Taxon;
use App\Form\Type\Admin\TaxonChoiceType;
use Sylius\Component\Taxonomy\Repository\TaxonRepositoryInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route(
 *     "/",
 *     options={"expose": true}
 * )
 */
class AdminController extends AbstractController
{
    /**
     * @Route("/admin/taxonAttributes", methods={"GET"})
     *
     * @param Request $request
     * @param TaxonRepositoryInterface $taxonRepository
     *
     * @return Response
     */
    public function renderTaxonAttributesAction(
        Request $request,
        TaxonRepositoryInterface $taxonRepository
    ): Response {
        $template = $request->attributes->get('template', 'Admin/Product/Attribute/taxon_attribute.html.twig');

        $form = $this->get('form.factory')->create(TaxonChoiceType::class, null, [
            'label' => 'sylius.form.product_attribute.taxon',
            'multiple' => true,
            'expanded' => true,
            'csrf_protection' => false,
        ]);

        /** @var Taxon[] $taxonCollection */
        $taxonCollection = $taxonRepository->findChildren(Taxon::ROOT_TAXON_CODE);

        foreach ($taxonCollection as $taxon) {
            /** @var ProductAttribute $attribute */
            foreach ($taxon->getAttributeList() as $attribute) {
                $taxonList[$taxon->getCode()][$attribute->getCode()] = $attribute->getName();
            }
        }

        return $this->render($template, [
            'form' => $form->createView(),
            'taxon_list' => $taxonList ?? [],
        ]);
    }
}
