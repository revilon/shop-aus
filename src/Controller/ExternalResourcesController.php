<?php

declare(strict_types=1);

namespace App\Controller;

use App\Component\YandexMarket\Generator\YmlGenerator;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route(
 *     "/",
 *     options={"expose": true}
 * )
 */
class ExternalResourcesController extends AbstractController
{
    /**
     * @Route("/price_list/yandex_market", methods={"GET"})
     *
     * @param YmlGenerator $ymlGenerator
     *
     * @return Response
     */
    public function priceListYandexMarket(YmlGenerator $ymlGenerator): Response
    {
        $yml = $ymlGenerator->generate();

        return new Response($yml);
    }
}
