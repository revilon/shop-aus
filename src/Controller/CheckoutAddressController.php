<?php

declare(strict_types=1);

namespace App\Controller;

use App\Component\Dadata\Exception\DadataException;
use App\UseCase\CustomerGuesserAddress\CustomerGuesserAddressHandler;
use App\UseCase\CustomerGuesserAddressList\CustomerGuesserAddressListHandler;
use App\UseCase\CustomerGuesserNameList\CustomerGuesserNameListHandler;
use App\UseCase\CustomerGuesserSurnameList\CustomerGuesserSurnameListHandler;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route(
 *     "/{_locale}",
 *     requirements={"_locale": "^[A-Za-z]{2,4}(_([A-Za-z]{4}|[0-9]{3}))?(_([A-Za-z]{2}|[0-9]{3}))?$"},
 *     options={"expose": true}
 * )
 */
class CheckoutAddressController extends AbstractController
{
    /**
     * @Route("/shop/api/checkoutAddress/customerGuesserNameList", methods={"GET"})
     *
     * @param CustomerGuesserNameListHandler $handler
     * @param Request $request
     *
     * @return JsonResponse
     *
     * @throws DadataException
     */
    public function customerGuesserNameList(CustomerGuesserNameListHandler $handler, Request $request): JsonResponse
    {
        $searchText = $request->get('searchText');

        if (!$searchText) {
            throw new BadRequestHttpException('Parameter "searchText" is empty');
        }

        $customerGuesserNameList = $handler->handle($searchText);

        return new JsonResponse([
            'name_list' => $customerGuesserNameList,
        ]);
    }

    /**
     * @Route("/shop/api/checkoutAddress/customerGuesserSurnameList", methods={"GET"})
     *
     * @param CustomerGuesserSurnameListHandler $handler
     * @param Request $request
     *
     * @return JsonResponse
     *
     * @throws DadataException
     */
    public function customerGuesserSurnameList(
        CustomerGuesserSurnameListHandler $handler,
        Request $request
    ): JsonResponse {
        $searchText = $request->get('searchText');

        if (!$searchText) {
            throw new BadRequestHttpException('Parameter "searchText" is empty');
        }

        $customerGuesserSurnameList = $handler->handle($searchText);

        return new JsonResponse([
            'surname_list' => $customerGuesserSurnameList,
        ]);
    }

    /**
     * @Route("/shop/api/checkoutAddress/customerGuesserAddressList", methods={"GET"})
     *
     * @param CustomerGuesserAddressListHandler $handler
     * @param Request $request
     *
     * @return JsonResponse
     *
     * @throws DadataException
     */
    public function customerGuesserAddressList(
        CustomerGuesserAddressListHandler $handler,
        Request $request
    ): JsonResponse {
        $searchText = $request->get('searchText');

        if (!$searchText) {
            throw new BadRequestHttpException('Parameter "searchText" is empty');
        }

        $customerGuesserAddressList = $handler->handle($searchText);

        return new JsonResponse([
            'address_list' => $customerGuesserAddressList,
        ]);
    }

    /**
     * @Route("/shop/api/checkoutAddress/customerGuesserAddress", methods={"GET"})
     *
     * @param CustomerGuesserAddressHandler $handler
     * @param Request $request
     *
     * @return JsonResponse
     *
     * @throws DadataException
     */
    public function customerGuesserAddress(CustomerGuesserAddressHandler $handler, Request $request): JsonResponse
    {
        $searchText = $request->get('searchText');

        if (!$searchText) {
            throw new BadRequestHttpException('Parameter "searchText" is empty');
        }

        $customerGuesserAddress = $handler->handle($searchText);

        return new JsonResponse($customerGuesserAddress);
    }
}
