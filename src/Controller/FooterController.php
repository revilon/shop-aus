<?php

declare(strict_types=1);

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route(
 *     "/{_locale}",
 *     requirements={"_locale": "^[A-Za-z]{2,4}(_([A-Za-z]{4}|[0-9]{3}))?(_([A-Za-z]{2}|[0-9]{3}))?$"},
 *     options={"expose": true}
 * )
 */
class FooterController extends AbstractController
{
    /**
     * @Route("/about", methods={"GET"})
     *
     * @return Response
     */
    public function about(): Response
    {
        return $this->render('Shop/about.html.twig');
    }

    /**
     * @Route("/terms_of_use", methods={"GET"})
     *
     * @return Response
     */
    public function termsOfUse(): Response
    {
        return $this->render('Shop/terms_of_use.html.twig');
    }

    /**
     * @Route("/public_offer", methods={"GET"})
     *
     * @return Response
     */
    public function publicOffer(): Response
    {
        return $this->render('Shop/public_offer.html.twig');
    }

    /**
     * @Route("/privacy_policy", methods={"GET"})
     *
     * @return Response
     */
    public function privacyPolicy(): Response
    {
        return $this->render('Shop/privacy_policy.html.twig');
    }

    /**
     * @Route("/payment_and_delivery", methods={"GET"})
     *
     * @return Response
     */
    public function paymentAndDelivery(): Response
    {
        return $this->render('Shop/payment_and_delivery.html.twig');
    }
}
