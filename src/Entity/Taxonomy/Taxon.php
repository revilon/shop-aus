<?php

declare(strict_types=1);

namespace App\Entity\Taxonomy;

use App\Entity\Product\ProductAttribute;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Sylius\Component\Core\Model\Taxon as BaseTaxon;
use Sylius\Component\Taxonomy\Model\TaxonTranslationInterface;

/**
 * @ORM\Entity
 * @ORM\Table(name="sylius_taxon")
 */
class Taxon extends BaseTaxon
{
    public const ROOT_TAXON_CODE = 'category';

    /**
     * @var Collection
     *
     * @ORM\ManyToMany(targetEntity=ProductAttribute::class, inversedBy="taxonList")
     */
    private $attributeList;

    public function __construct()
    {
        parent::__construct();

        $this->attributeList = new ArrayCollection();
    }

    /**
     * @param ProductAttribute $attribute
     */
    public function addAttribute(ProductAttribute $attribute): void
    {
        if ($this->attributeList->contains($attribute)) {
            return;
        }

        $this->attributeList->add($attribute);

        $attribute->addTaxon($this);
    }

    /**
     * @param ProductAttribute $attribute
     */
    public function removeAttribute(ProductAttribute $attribute): void
    {
        if (!$this->attributeList->contains($attribute)) {
            return;
        }

        $this->attributeList->removeElement($attribute);

        $attribute->removeTaxon($this);
    }

    /**
     * @return Collection
     */
    public function getAttributeList(): Collection
    {
        return $this->attributeList;
    }

    /**
     * @param Collection $attributeList
     */
    public function setAttributeList(Collection $attributeList): void
    {
        $this->attributeList = $attributeList;
    }

    /**
     * {@inheritDoc}
     */
    protected function createTranslation(): TaxonTranslationInterface
    {
        return new TaxonTranslation();
    }
}
