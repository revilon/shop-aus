<?php

declare(strict_types=1);

namespace App\Entity\Product;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Sylius\Component\Attribute\AttributeType\SelectAttributeType;
use Sylius\Component\Product\Model\ProductAttributeValue as BaseProductAttributeValue;
use Sylius\Component\Attribute\Model\AttributeValueInterface;
use Sylius\Component\Attribute\AttributeType\DateAttributeType;
use Sylius\Component\Attribute\AttributeType\DatetimeAttributeType;
use Sylius\Component\Attribute\AttributeType\PercentAttributeType;

/**
 * @ORM\Entity
 * @ORM\Table(name="sylius_product_attribute_value")
 */
class ProductAttributeValue extends BaseProductAttributeValue
{
    /**
     * @param string|null $locale
     *
     * @return array
     */
    public function getHumanValueList(?string $locale = null): array
    {
        $valueList = [];

        if (null === $this->attribute) {
            return null;
        }

        $getter = 'get' . $this->attribute->getStorageType();
        $attributeValue = $this->$getter();

        $isJsonStorage = $this->attribute->getStorageType() === AttributeValueInterface::STORAGE_JSON;
        $isSelectType = $this->attribute->getType() === SelectAttributeType::TYPE;

        if ($isJsonStorage && $isSelectType) {
            $locale = $this->attribute->getTranslation($locale)->getLocale();

            foreach ($attributeValue as $choiceId) {
                $valueList[] = $this->attribute->getConfiguration()['choices'][$choiceId][$locale];
            }

            return $valueList;
        }

        $isDateStorage = $this->attribute->getStorageType() === AttributeValueInterface::STORAGE_DATE;
        $isDateType = $this->attribute->getType() === DateAttributeType::TYPE;

        if ($isDateStorage && $isDateType) {
            /** @var DateTime $attributeValue */
            return [$attributeValue->format('d.m.Y')];
        }

        $isDateTimeStorage = $this->attribute->getStorageType() === AttributeValueInterface::STORAGE_DATETIME;
        $isDateTimeType = $this->attribute->getType() === DatetimeAttributeType::TYPE;

        if ($isDateTimeStorage && $isDateTimeType) {
            /** @var DateTime $attributeValue */
            return [$attributeValue->format('d.m.Y H:i:s')];
        }

        if (is_bool($attributeValue)) {
            return $attributeValue ? ['+'] : ['-'];
        }

        $isFloatStorage = $this->attribute->getStorageType() === AttributeValueInterface::STORAGE_FLOAT;
        $isPercentType = $this->attribute->getType() === PercentAttributeType::TYPE;

        if ($isFloatStorage && $isPercentType) {
            return [($attributeValue * 100) . '%'];
        }

        return [$this->$getter()];
    }
}
