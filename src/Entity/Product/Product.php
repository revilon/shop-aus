<?php

declare(strict_types=1);

namespace App\Entity\Product;

use Doctrine\ORM\Mapping as ORM;
use Sylius\Component\Core\Model\Product as BaseProduct;
use Sylius\Component\Product\Model\ProductTranslationInterface;

/**
 * @ORM\Entity
 * @ORM\Table(name="sylius_product")
 */
class Product extends BaseProduct
{
    /**
     * @return string|null
     */
    public function getSecondHandDescription(): ?string
    {
        /** @var ProductTranslation $translation */
        $translation = $this->getTranslation();

        return $translation->getSecondHandDescription();
    }

    /**
     * @param string|null $secondHandDescription
     */
    public function setSecondHandDescription(?string $secondHandDescription): void
    {
        /** @var ProductTranslation $translation */
        $translation = $this->getTranslation();

        $translation->setSecondHandDescription($secondHandDescription);
    }

    /**
     * {@inheritDoc}
     */
    protected function createTranslation(): ProductTranslationInterface
    {
        return new ProductTranslation();
    }
}
