<?php

declare(strict_types=1);

namespace App\Entity\Product;

use Doctrine\ORM\Mapping as ORM;
use Sylius\Component\Core\Model\ProductTranslation as BaseProductTranslation;

/**
 * @ORM\Entity
 * @ORM\Table(name="sylius_product_translation")
 */
class ProductTranslation extends BaseProductTranslation
{
    /**
     * @var string|null
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $secondHandDescription;

    /**
     * @return string|null
     */
    public function getSecondHandDescription(): ?string
    {
        return $this->secondHandDescription;
    }

    /**
     * @param string|null $secondHandDescription
     */
    public function setSecondHandDescription(?string $secondHandDescription): void
    {
        $this->secondHandDescription = $secondHandDescription;
    }
}
