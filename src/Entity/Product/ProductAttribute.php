<?php

declare(strict_types=1);

namespace App\Entity\Product;

use App\Entity\Taxonomy\Taxon;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Sylius\Component\Attribute\Model\AttributeTranslationInterface;
use Sylius\Component\Product\Model\ProductAttribute as BaseProductAttribute;

/**
 * @ORM\Entity
 * @ORM\Table(name="sylius_product_attribute")
 */
class ProductAttribute extends BaseProductAttribute
{
    public const SECOND_HAND = 'second_hand';

    /**
     * @var Collection
     *
     * @ORM\ManyToMany(targetEntity=Taxon::class, mappedBy="attributeList", cascade={"persist"})
     * @ORM\JoinTable(name="TaxonAttribute")
     */
    private $taxonList;

    public function __construct()
    {
        parent::__construct();

        $this->taxonList = new ArrayCollection();
    }

    /**
     * @param Taxon $taxon
     */
    public function addTaxon(Taxon $taxon): void
    {
        if ($this->taxonList->contains($taxon)) {
            return;
        }

        $this->taxonList->add($taxon);

        $taxon->addAttribute($this);
    }

    /**
     * @param Taxon $taxon
     */
    public function removeTaxon(Taxon $taxon): void
    {
        if (!$this->taxonList->contains($taxon)) {
            return;
        }

        $this->taxonList->removeElement($taxon);

        $taxon->removeAttribute($this);
    }

    /**
     * @return Collection
     */
    public function getTaxonList(): Collection
    {
        return $this->taxonList;
    }

    /**
     * @param Collection $taxonList
     */
    public function setTaxonList(Collection $taxonList): void
    {
        $this->taxonList = $taxonList;

        /** @var Taxon $taxon */
        foreach ($this->taxonList as $taxon) {
            $taxon->addAttribute($this);
        }
    }

    /**
     * {@inheritDoc}
     */
    protected function createTranslation(): AttributeTranslationInterface
    {
        return new ProductAttributeTranslation();
    }
}
