require('jquery-typeahead/dist/jquery.typeahead.min.css');
require('jquery-typeahead');

jQuery(function ($) {
    let form_address = $('form[name=sylius_address]');

    form_address.find('input[name="sylius_address[firstName]"]').typeahead({
        minLength: 1,
        maxItem: 10,
        dynamic: true,
        filter: false,
        cancelButton: false,
        source: {
            name_list: {
                ajax: {
                    url: Routing.generate('app_checkoutaddress_customerguessernamelist', { '_locale': REQUEST_LOCALE }),
                    data: {
                        searchText: '{{query}}'
                    },
                    path: 'name_list'
                }
            },
        }
    });

    form_address.find('input[name="sylius_address[lastName]"]').typeahead({
        minLength: 1,
        maxItem: 10,
        dynamic: true,
        filter: false,
        cancelButton: false,
        source: {
            surname_list: {
                ajax: {
                    url: Routing.generate('app_checkoutaddress_customerguessersurnamelist', { '_locale': REQUEST_LOCALE }),
                    data: {
                        searchText: '{{query}}'
                    },
                    path: 'surname_list'
                }
            },
        }
    });

    form_address.find('input[name="sylius_address[inlineAddress]"]').typeahead({
        minLength: 1,
        maxItem: 10,
        dynamic: true,
        filter: false,
        cancelButton: false,
        source: {
            address_list: {
                ajax: {
                    url: Routing.generate('app_checkoutaddress_customerguesseraddresslist', { '_locale': REQUEST_LOCALE }),
                    data: {
                        searchText: '{{query}}'
                    },
                    path: 'address_list'
                }
            },
        },
        callback: {
            onClickAfter: function (node, a, item, event) {
                form_address.addClass('loading');
                $.ajax({
                    method: 'GET',
                    url: Routing.generate('app_checkoutaddress_customerguesseraddress', { '_locale': REQUEST_LOCALE }),
                    data: {searchText: item['display']},
                    dataType: 'json',
                }).done(function (data) {
                    form_address.removeClass('loading');
                    form_address
                        .find('input[name="sylius_address[street]"]')
                        .val(data['address'])
                    ;
                    form_address
                        .find('input[name="sylius_address[provinceName]"]')
                        .val(data['region'])
                    ;
                    form_address
                        .find('input[name="sylius_address[city]"]')
                        .val(data['city'])
                    ;
                    form_address
                        .find('input[name="sylius_address[postcode]"]')
                        .val(data['postalCode'])
                    ;
                }).fail(function (jqXHR) {
                    form_address.removeClass('loading');
                });
            },
        }
    });
});
