const $ = require('jquery');
const routes = require('../../public/fos_js_routes.json');
import Routing from '../../vendor/friendsofsymfony/jsrouting-bundle/Resources/public/js/router.min.js';
Routing.setRoutingData(routes);

require('./shop/checkout/address/shippingAddressForm');
require('./shop/profile/address/shippingAddressForm');

global.$ = global.jQuery = $;
global.Routing = Routing;
