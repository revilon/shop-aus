import jQuery from 'jquery';
import 'semantic-ui-css/components/dropdown';

jQuery(function ($) {
    $('#sylius_taxon_choice').change(function (event) {
        let attribute_list = {};

        $(event.target).closest('#sylius_taxon_choice').find(':checked').each(function (key, item) {
            let taxon_code = $(item).val();

            for (let attribute_code in taxon_list[taxon_code]) {
                if (!taxon_list[taxon_code].hasOwnProperty(attribute_code) ) {
                    continue;
                }

                attribute_list[attribute_code] = attribute_code;
            }
        });

        let dropdown_attribute_list = $('#attributeChoice .menu [data-value]');

        dropdown_attribute_list.hide();

        dropdown_attribute_list.filter(function (index) {
            return attribute_list.hasOwnProperty($(this).data('value'));
        }).show();
    });

    $('div#attributeChoice > .ui.dropdown.search').dropdown({
        onRemove: function (removedValue) {
            $('#attributesContainer .attribute[data-id="' + removedValue + '"').remove();
        },
        forceSelection: false,
        fullTextSearch: true,
    });
});
