let Encore = require('@symfony/webpack-encore');

if (!Encore.isRuntimeEnvironmentConfigured()) {
    Encore.configureRuntimeEnvironment(process.env.NODE_ENV || 'dev');
}

Encore
    .setOutputPath('public/build/')
    .setPublicPath('/build')
    .autoProvidejQuery()
    .addEntry('shop', './assets/js/shop.js')
    .addEntry('admin', './assets/js/admin.js')
    .splitEntryChunks()
    .enableSingleRuntimeChunk()
    .cleanupOutputBeforeBuild()
    .enableSourceMaps(!Encore.isProduction())
    .enableVersioning(Encore.isProduction())
;

if (Encore.isProduction()) {
    Encore.copyFiles([
        {
            from: 'assets/img',
            to: 'img/[path][name].[hash:8].[ext]'
        }
    ]);
} else {
    Encore.copyFiles([
        {
            from: 'assets/img',
            to: 'img/[path][name].[ext]'
        }
    ]);
}

const config = Encore.getWebpackConfig();

if (!Encore.isProduction()) {
    config.stats.errors = true;
    config.stats.warnings = true;
}

module.exports = config;
